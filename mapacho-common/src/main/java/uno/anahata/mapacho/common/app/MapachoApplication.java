/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.common.app;

import java.io.*;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import uno.anahata.mapacho.common.runtime.JRE;

/**
 *
 * @author pablo
 */
@Getter
@Setter
public class MapachoApplication implements Serializable {

    private URL codeBase;

    private String displayName;

    private String displayVer;

    private String splash;

    private String preloaderClass;

    private JRE jre;

    private List<MapachoArtifact> classpath = new ArrayList();

    private Map<String, String> properties = new HashMap();

    private String jvmArgs = "";

    private String mainClass;

    private List<String> arguments = new ArrayList();
    
    public List<MapachoArtifact> getMatchingArtifacts() {
        return classpath.stream().filter(a -> a.matches()).collect(Collectors.toList());
    }

    public List<MapachoArtifact> getMatchingLibPath() {
        return getMatchingArtifacts().stream().filter(a -> a.isNativeLib()).collect(Collectors.toList());
    }

    public List<MapachoArtifact> getMatchingClassPath() {
        return getMatchingArtifacts().stream().filter(a -> !a.isNativeLib()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        String base = "MapachoApplication{" 
                + "codeBase=" + codeBase + ",\n\n displayName=" + displayName + ",\n displayVer=" + displayVer 
                + ",\n splash=" + splash + ",\n preloaderClass=" + preloaderClass + ",\n jre=" + jre 
                + ",\n properties=" + properties + ",\n jvmArgs=" + jvmArgs 
                + ",\n mainClass=" + mainClass + ",\n arguments=" + arguments
                + ",\n classpath=" + classpath.size();
        for (MapachoArtifact mapachoArtifact : classpath) {
            base += ",\n     " + mapachoArtifact.getJarFileName();
        }
        base += "}";
        return base;
                
                
    }

}
