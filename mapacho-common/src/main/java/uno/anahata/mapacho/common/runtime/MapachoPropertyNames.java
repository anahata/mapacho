/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.common.runtime;

/**
 *
 * @author pablo
 */
public class MapachoPropertyNames {
    public static final String DESCRIPTOR = "mapacho.descriptor";
    public static final String DESCRIPTOR_TIMESTAMP = "mapacho.descriptor.timestamp";
    public static final String CODEBASE = "mapacho.codebase";    
    public static final String CONTEXT = "mapacho.context";
    public static final String SPLASH_SCREEN = "mapacho.splash";
    public static final String PRELOADER_PORT = "mapacho.preloader.port";
    
}
