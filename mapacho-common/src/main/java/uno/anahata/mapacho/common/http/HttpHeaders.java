/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.common.http;

/**
 *
 * @author pablo
 */
public final class HttpHeaders {
    public static final String HEADER_LASTMOD = "Last-Modified";
    
    public static final String HEADER_REAL_CONTENT_LENGTH = "Real-Content-Length";

    public static final String GZIP_ENCODING = "gzip";
    //public static final String PACK200_GZIP_ENCODING = "pack200-gzip";
    
    public static final String HEADER_CONTENT_ENCODING = "Content-Encoding";

    public static final String HEADER_JNLP_VERSION = "x-java-jnlp-version-id";

    //public static final String PACK200_MIME_TYPE = "application/x-java-pack200";

    //https://docs.oracle.com/javase/8/docs/technotes/guides/javaws/developersguide/downloadservletguide.html
    public static final String JNLP_MIMETYPE = "application/x-java-jnlp";

    public static final String JNLP_ERROR_MIMETYPE = "application/x-java-jnlp-error";

    public static final String JAR_MIMETYPE = "application/x-java-archive";

    public static final String JARDIFF_MIMETYPE = "application/x-java-archive-diff";

}
