package uno.anahata.mapacho.common.os;

/*
 * #%L
 * anahata-util
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Provides information about the operating system the JVM is running in.
 *
 * @author Robert Nagajek
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class OSUtils {
    private static final String SYSPROP_OS_NAME = "os.name";
    
    private static final String SYSPROP_OS_USER_NAME = "user.name";

    private static final String SYSPROP_OS_USER_HOME = "user.home";

    private static final String OS_NAME_UNKNOWN = "unknown";

    private static final String OS_NAME_MAC_1 = "mac";

    private static final String OS_NAME_MAC_2 = "darwin";

    private static final String OS_NAME_WINDOWS = "windows";

    private static final String OS_NAME_LINUX = "linux";

    
    /**
     * Determine if running under Windows.
     *
     * @return true if running under Windows, false if not.
     */
    public static boolean isWindows() {        
        return isWindows(getOsName());
    }
    
    /**
     * Determine if running under Windows.
     *
     * @return true if running under Windows, false if not.
     */
    public static boolean isWindows(String osName) {
        return osName.toLowerCase().startsWith(OS_NAME_WINDOWS);
    }

    public static boolean isMac() {        
        return isMac(getOsName());
    }
    /**
     * Determine if running under Mac OS.
     *
     * @return true if running under Mac OS, false if not.
     */
    public static boolean isMac(String osName) {        
        return osName.toLowerCase().startsWith(OS_NAME_MAC_1) || osName.toLowerCase().startsWith(OS_NAME_MAC_2);
    }

    /**
     * Determine if running under Linux.
     *
     * @return true if running under Linux, false if not.
     */
    public static boolean isLinux(String osName) {        
        return osName.toLowerCase().startsWith(OS_NAME_LINUX);
    }
    
    /**
     * Determine if running under Linux.
     *
     * @return true if running under Linux, false if not.
     */
    public static boolean isLinux() {
        final String osName = getOsName();
        return isLinux(osName);
    }

    public static String getOsName() {
        return System.getProperty(SYSPROP_OS_NAME, OS_NAME_UNKNOWN).toLowerCase();
    }

    /**
     * Gets the user name system property.
     *
     * @return the user name system property.
     */
    public static String getUserName() {
        return System.getProperties().getProperty(SYSPROP_OS_USER_NAME);
    }

    /**
     * Gets the user home directory system property.
     *
     * @return the user home system property.
     */
    public static String getUserHome() {
        return System.getProperties().getProperty(SYSPROP_OS_USER_HOME);
    }

}
