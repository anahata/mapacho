/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.common.http;

/**
 *
 * @author pablo
 */
public final class HttpParameters {
    // Arguments
    public static final String ARG_ARCH = "arch";

    public static final String ARG_OS = "os";

    public static final String ARG_LOCALE = "locale";

    public static final String ARG_VERSION_ID = "version-id";

    public static final String ARG_CURRENT_VERSION_ID = "current-version-id";

    public static final String ARG_PLATFORM_VERSION_ID = "platform-version-id";

    public static final String ARG_KNOWN_PLATFORMS = "known-platforms";

}
