/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.common.art;

/**
 *
 * @author pablo
 */
public class Mapachito {
    public static final String ASCII = "           `     `  `...`              `                               \n" +
"                      `   ```++/o+/:-`    `   ``      `            `            \n" +
"                    ``  `++`/s.oosso+o:`       `        `        `.             \n" +
"                     ` ``+s-/+-:/yo.-yds/`     `-++:.    ` `           `        \n" +
"                      ````-///++o+--sdmmhs:.   -sssoo:`    `  ``       ``       \n" +
"                  ` `` -/++os/syso/+soshmmhso/::ososhho:.   /so:.   `` `        \n" +
"            ``  ` ``  -o+-..-/s+:.:oy+oydddhddddhhdddddso+:/ysyo:`       `      \n" +
"                   `  .++/...-::+shdmmmmmNNmmmmmmNNNmmmmmdhhdhso-`   `  `. ` `` \n" +
"                      ``oydyyyhdmmmmmNNNNNNmmmmmmmNmNNNNNNNNmmhyss+:.`` `` ```  \n" +
"               .--.` `.-syddmmmmmmmmNNNmmddddmmdmmmmNNNNNNNNNNNNmmdys+-```````  \n" +
"              -+ooos++shhdmmmmmmmmmmddddmmddddmmmdddNNNNNNNNNNNNNNNmdhs+.`` ``` \n" +
"              `...-syshdmmmmmmmmmmmmdmmhhdmmmmdddmNmNNNNNNNNNNNNNNNNNmdss-`   ``\n" +
"               ` `:sydmmmmmmmmmmmmmmmdyddmdhddddyodNNNNNNNNNddddmmNNNNNmsy.`````\n" +
"           `.   -oyhmmmmmmmmmmmmmmmmm+yhmyyyymNmhdmNNNNNNNNNhNNmddhdNNNNmss`````\n" +
"         `-+/-`/yhdmmmmmmmmmmmmmmmmmdosddhyyhmddNNNNNNNNNNNNyyyhdddyydNNNmh`````\n" +
"         `/o++shhmmmmmmmmmmmmmmmmddmmmmmdddddmmmNNNNNNNNNNNNhyhmhhmNmshNNNh.````\n" +
"          `.-yshmmmmmmmmmmmmmmmmhdmmmmmmmmmmmdddddhhNNNNNNNNmmdhyyshNmyhNNy.````\n" +
"           `:syddmmmmmmmmmmmmmmhmmmmmmmmmmNmhmhhhdmdNmhddNNNmydmshdhhNhhmdo`````\n" +
"          -/shddmmmmmmmmmmmmmmhdmmmmmmmmmmmmdddddddddyyddmmmNmsddydmdhhdmy-`````\n" +
"          ooyddmmmmmmmmmmmmmmdhmmmmmmmmmmmmmmdddddmmNmhohdddhmmymyddyomhh/````.`\n" +
"        `.+sdddmmmmmmmmmmmmmmhmmmmmmmmmmmdyydhdosyssyyyyhhdmNddddmNNNhsy.````.``\n" +
"      ` .+oddmmmmmmmmmmmmmmmmhmmmmmmmmmhddhhmddhhhsmdymmydsssmhmNNNNNNy:````````\n" +
"     ` `:odddddmmmmmmmmmmmmmmhmmmmmmdhhddmmmmNNNNNddyhhhyhyyyymhmNNNNNh:````````\n" +
"  `  ``/+sddddmmmmmmmmmmmmmmhmmmmdhhddmmmmmmmNNNNNNNNmmmNNNNmdhmyNNNNNho``````.`\n" +
"     ``+oddmmmmmmmmmmmmmmmmmymmhyshmmmmmmmNmmNNNNNNNNNNNNNNNNNmyyhNNNNys``.`````\n" +
"      :ooddddmmmmmmmmmmmmmmdhhyyhmmmmmmmmmmNNNNNNNNNmNNNNNNNNNNydhNNNNhy````````\n" +
"     `:+syddmmmmmmmmmmmmmmmhdddhhmmmmmmmmNNNNmNmNmNNNNNNNNNNNNNmhmhNNNyy``.`````\n" +
"     .//yhyhdmmmmmmmmmmmmmmmddmyhhdmmmmmmmNNNNmmmNNNNNNNNNNNNNNNmysdNNys````````\n" +
"    ./sshyddhhhdddmmmmhhhhydmmhdhhhhdmmdmmmmmNNNmNNNNNNNNNNNNNNNNmhhmNyo..``` ``\n" +
"     -s/ydhhhdddhyyddydmmmmoyhmhmhdmdh/osyoshyhhhhhmmdmNNNNNNNNNNmhhydy-```` ```\n" +
"      ++::shddhhyhmmydmmmms:`+yhhmmhdmmsoyyohssh+mm+om+syyhosyyhyodNdsh````   ``\n" +
"      :odho/:/+sy+omymmmmds-  .oysmmdhmmmhhhhhhhoyhssyshsys+oodyhdhy+:``````  ``\n" +
"      /+dddddhyo+/smymmmmyhs`  `:symmmhdmmmmmmmmdhyhdddhhhdNNdymd+-``````````  `\n" +
"      oshdddddmmdyymymmmmsyyy+:---:sdmmmhddmmmmmmmmymmmmNNNdhmdo-.````````````  \n" +
"     `+ysdddddmddsydshmmhoyyddssyy/-:sdmmmmddddmmmmhmmmmmddmds-``````````.````  \n" +
"      .oosdddddddoshhsdmy+`yyyyyso/:``:sdmmmmmmdddydddddmmho-`````.`````````` ``\n" +
"       `oyydddddyy+yoyhyoo -oyyshs-   ``:ohdmmmmmmmmmmdds/.```````````````` `` `\n" +
"        :hshdddydsoyysysys  `+o-.-.  `````./ooshdhyyso/-.```````    ``` `  `  ``\n" +
"        `/ysdddyyhdoydmdds   :yss+:-```````````-/:/osssoo````   ````````````````\n" +
"         `+yydddmmmhhmmmmo`  ohysysh/````````.:+yhyysooyy-`    `````````````````\n" +
"`         `/yymmmmmmhhmmmh- `odsshyyo: `.:+syysddhhyyssyss.   ``````````````````\n" +
"`   `      `ohhdmmmmmsdmmms-/ommhsyy-./yhddhhymdsyhddmydyys/.```..`````````````.\n" +
"    -`     `/yyhmdmmh-/dmmdyysdh+-.:/shhhddmmmmmmmmddhdmmmmmy+:....`````````````\n" +
"`   ++`  ` ``/sysys+..symmmdsdy:`.+ddshyyhmhsddddmmmmmdhmmddmdy+-::-````````````\n" +
"    os/   `` `.//-:/ohmdymmmddo.:ymdhsyyhsyyydddhyhddmmdhdhhhhdyysyys:``````````\n" +
"    oso  ``    `+yydddhyyymmmmhssddymmhyyhdshhysohydsmmmmsmmdhddhsysdo.`````` ``\n" +
"`   .sy` ``   ` `/yhsyyyhdhdddhhhhdyhdmmmyhyhsssshymhmmNNsmNNhyhhydso.``````````\n" +
"```  .++-`   `` `-+yyydmmmmddddhddyhdyshhhhsodhhhyhhhmNNNdhmmyhdNhys.....`.`` ``\n" +
"  `   `+o+.`  `    -ohydmmmmmmsdmmdyy++oshmmdhdhhhyymmNNmm:/:-:ohss//``.``.   ``\n" +
"       `/yo.        `oyhhdmmmmmhydyhohsyyhmdsyhssoysmmNNNhshhy/.-s:.-`````   ``.\n" +
"         :so.       .-:+sshdmmmmmhsoyhsmmmmdhddsoshshmmNdsmNNNmo/y`-.`..-`  ````\n" +
"          os/           `-oyhdmmmmmdhhyhddhhhdhydsyddmmy:hmmmmd++:`.``.-.. ```.`\n" +
"          :yy.             -hshdmmmmmmmdhhddyhddhhhdhhd+`-mmmys/-`-`:.:..`  ````\n" +
"     `    -yh:              oymdshdmmmmmmmmmmdhhddhdmmmo`.dsoo-`````.:````` ````\n" +
"    `.    `oho             `shmmsmdhhhdddmmmmmmmmmmmmmd:.:/:.``````````.-```````\n" +
"          `.sy-     ` `    `odmmsmmmmmdo+sosoosyyyhyoo+-`````````````````````` `\n" +
"          ``:ss       `     ohmmomddddmys:-----:/:--.```````````````````````````\n" +
"          -`.ss           ``+smmodmddmdyy:  ``.-:////```````````````````````````\n" +
"            `ys  ` `.`   -ssyodmshddddmhy+.:+sssssys/` `````````````````````````\n" +
"     ``    .`hy` `.`-`.:/syhyyyhyydddddmysydymmdhsy+-` `` ``````````````````````\n" +
"    `       /ss ..:.:o+/hhyshyshhdmmddmmhhds+ossysyo+- `````````````````````````\n" +
"   .:      .ho:.  -/+yosoyyo:-sddsmmmmmmmho-  -sho/``` `````````````````````````\n" +
"  `   .` `:ss.`--`` -sds/sdy/ :yssommmmmmdo+` `````    `````````````````````````\n" +
"      ` `-so`  `.`  `:s+.-o+```+:.-sdmmmmdy/+:` +.:/`  `````````` ``````````````\n" +
"        /yo`.  ` ```  .`  `     /ymdhhmdmmmh-o-`ooys`  ``````````` `````````````\n" +
"      `+os` `                   `+hmmdhdmmmmyss+ymdho:- `   ``  ``  ````````````\n" +
"     -ooy.                     `./sshhhsmmmmmhshmhshydo-`          `` ``````````\n" +
"    -s:h+                 ```./o+shhmsyyhmmmmmdmm+hhoysy+`         `  `  ```````\n" +
"   `oo+d.                 `:oyoyossyssyhsymmmmmdmyosossy+`     `      ``` ``````\n" +
"   /oooy                  -//o+ydsydh+ssysymmmddmmys-`.-`      ````   `      ```\n" +
"  `oysss     ``            `..+os/oo+`:o/+/dmmdddmmyo.          ``    ``     ```\n" +
"  /oysso     `                .-. ```  /...ommdddmmdyo-       `                `\n" +
"  +hysoo           `     `          `.-+ohhyhmmmmmmmdyo-                `   `` `\n" +
" `ohysos           ``` `````    ..:+oohdhhhdsdmdmmmmmhy+..-:`              ` `` \n" +
" .ohhsss           `````````` :+shyssshdmmyssyddddmmmmyhoodo:                .` \n" +
" `ohhshs.              `````` `-sy+ysysohy+ysdddddddddyshmmho`                  \n" +
"  +hhoho:               ````    osyhyyyy+/sdsdddddddmhhdysdmyo.               ` \n" +
"  /yhsys/                ` `    //+oshhs.:syyddmddddmdyososhyh+                 \n" +
"  .ohhsh+`                ` `   `` `:+o/`oysddddddddddoooyhssh+                 \n" +
"   +shsys/                           `..+shsmddddddddyyoyssoss:                 \n" +
"   .ohhoho:                        ``./+ohsdmdddddddhsy+-..`.:`   `` `        ``\n" +
"    -ohhsyo:`                    `-o+ossyshmmdddmmmdyy/`          ``  -`        \n" +
"     :shhsso//:.`            `.-/+osyddyydmmmmmmmmhyh+.             `           \n" +
"      /yhhysoooo//-.``   ``-:///shdddyydddmmmmmmdyys:.              `           \n" +
"      `/yhhhyssoo+o+++-:/:/+// `yhyyhddddddddddhyyo.                          ` \n" +
"       `/yhhhhhhyssoo++oyhyo++:+yhddddddddddhs+os/`                           ` \n" +
"        `/yhhhhhhhhhhyyyyyhhhdddddddddhdhhy/`-+/.                          ` `  \n" +
"          :yhhhhhhhhddddddhhdddddddddhyssooo--`                                `\n" +
"           -oyhhhhhhhhhhdhhhdddhhyso++ooo/.          `         `          ``  ``\n" +
"            `:+oossyyyyyyssso++++o+//:-`                                  ``  ``\n" +
"               `.-:/++++/+////:-.`                                     .    ` ``\n" +
"                   `   `                                    .    `            ` ";
}
