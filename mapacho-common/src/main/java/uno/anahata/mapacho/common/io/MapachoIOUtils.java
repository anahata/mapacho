/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.common.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pablo
 */
public class MapachoIOUtils {

    /**
     * Extracts a jar file onto a directory.
     *
     * @param jarFile
     * @param destDir
     * @throws IOException
     */
    public static void extractJarFile(File jarFile, File destDir) throws IOException {
        try (java.util.jar.JarFile jar = new java.util.jar.JarFile(jarFile)) {
            java.util.Enumeration enumEntries = jar.entries();
            while (enumEntries.hasMoreElements()) {
                java.util.jar.JarEntry file = (java.util.jar.JarEntry)enumEntries.nextElement();
                java.io.File f = new java.io.File(destDir + java.io.File.separator + file.getName());
                if (file.isDirectory()) { // if its a directory, create it
                    f.mkdirs();
                    continue;
                } else {
                    f.getParentFile().mkdirs();
                }

                try (java.io.InputStream is = jar.getInputStream(file)) {
                    try (java.io.FileOutputStream fos = new java.io.FileOutputStream(f)) {
                        while (is.available() > 0) {  // write contents of 'is' to 'fos'
                            fos.write(is.read());
                        }
                    }
                }
            }
        }
    }
    
    public static void putInJar(InputStream is, File jar, String path) throws IOException {

        /* Create ZIP file System */
        try (FileSystem zipfs = getZipFileSystem(jar)) {
            /* Get the Path inside ZIP File to delete the ZIP Entry */
            Path pathInZipfile = zipfs.getPath(path);
            /* Execute Delete */
            if (Files.exists(pathInZipfile)) {

                System.out.println("About to delete  " + pathInZipfile);

                Files.delete(pathInZipfile);
            }

            if (is != null) {
                System.out.println("Copying " + is + " to " + pathInZipfile);
                Files.copy(is, pathInZipfile);
            }

        }
    }

    public static FileSystem getZipFileSystem(File jar) throws IOException {
        Map<String, String> zip_properties = new HashMap<>();
        /* We want to read an existing ZIP File, so we set this to False */
        zip_properties.put("create", "false");

        /* Specify the path to the ZIP File that you want to read as a File System */
        String absolutePath = jar.getAbsolutePath();
        if (!absolutePath.startsWith("/")) {
            absolutePath = "///" + absolutePath.replace("\\", "/");
        }
        URI zip_disk = URI.create("jar:file:" + absolutePath);

        /* Create ZIP file System */
        return FileSystems.newFileSystem(zip_disk, zip_properties);
    }
}
