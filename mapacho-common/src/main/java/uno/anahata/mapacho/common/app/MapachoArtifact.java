/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.common.app;

import java.io.File;
import java.io.Serializable;
import java.util.Locale;
import lombok.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import uno.anahata.mapacho.common.runtime.JRE;
import static org.apache.commons.lang3.StringUtils.*;

/**
 *
 * @author pablo
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MapachoArtifact implements Serializable {

    public static final String CLASSIFIER_TOKEN = "__";

    public static final String OS_TOKEN = "O";

    public static final String ARCH_TOKEN = "A";

    public static final String LOCALTE_TOKEN = "L";

    public static final String VERSION_TOKEN = "V";

    private MapachoApplication application;

    private String name = "";

    private String os = "";

    private String arch = "";

    private String locale = "";

    private String version = "";

    private boolean nativeLib = false;

    private transient File file;

    public final String getBaseFileName() {
        String ret = name;
        ret += addClassifier(OS_TOKEN, os);
        ret += addClassifier(ARCH_TOKEN, arch);
        ret += addClassifier(LOCALTE_TOKEN, locale);
        ret += addClassifier(VERSION_TOKEN, version);
        return ret;
    }

    private static String addClassifier(String classifier, String value) {
        String ret = "";
        if (!isBlank(value)) {
            ret += CLASSIFIER_TOKEN + classifier + value;
        }
        return ret;
    }

    public final String getJarFileName() {
        return getBaseFileName() + ".jar";
    }

    public final String getJarGzFileName() {
        return getJarFileName() + ".gz";
    }


    public String getJnlpDependencyString(String libDir, boolean main, String download) {

        String ret = "<jar href=\"" + libDir + "/" + name + ".jar\" version=\"" + version + "\"";

        if (!StringUtils.isBlank(download)) {
            ret += " download=\"" + download + "\"";
        }

        if (main) {
            ret += " main=\"true\"";
        }

        ret += "/>";

        return ret;
    }

    public File getLibDir(File repo) throws Exception {
        return new File(repo, getBaseFileName());
    }

    public File getFile(File repo) throws Exception {
        return new File(repo, getJarFileName());
    }

    public static MapachoArtifact fromFile(MapachoApplication application, File f) {

        MapachoArtifact ret = new MapachoArtifact();
        ret.setApplication(application);
        ret.setFile(f);

        //look at the 
        String[] chunks = FilenameUtils.getBaseName(f.getName()).split(CLASSIFIER_TOKEN);
        String base = chunks[0];
        ret.setName(base);
        
        for (int i = 1; i < chunks.length; i++) {
            String chunk = chunks[i];
            String classifier = chunk.substring(0, 1);
            String value = chunk.substring(1);

            /*
            if (value.contains(".")) {//remove extension
                value = value.substring(0, value.indexOf("."));
            }
            */

            if (classifier.equals(OS_TOKEN)) {
                ret.setOs(value);
            } else if (classifier.equals(ARCH_TOKEN)) {
                ret.setArch(value);
            } else if (classifier.equals(LOCALTE_TOKEN)) {
                ret.setLocale(value);
            } else if (classifier.equals(VERSION_TOKEN)) {
                ret.setVersion(value);
            } else {
                throw new IllegalStateException(
                        "Could not parse token classifier=" + classifier + "=" + value + " in " + f);
            }
        }
        return ret;
    }

    private static boolean equals(String s1, String s2) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }
        return s1.trim().toLowerCase().equalsIgnoreCase(s2.trim().toLowerCase());
    }

    private static boolean matches(String s1, String s2) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }
        return s1.trim().toLowerCase().startsWith(s2.trim().toLowerCase());
    }

    private boolean isSameArtifactButDifferentVersion(MapachoArtifact ma) {
        return equals(name, ma.getName())
                && equals(os, ma.getOs())
                && equals(arch, ma.getArch())
                && equals(locale, ma.getLocale());
        //&& equals(version, ma.getVersion());
    }

    public MapachoArtifact getBestCurrentVersionMatch(File repo) throws Exception {
        System.out.println("Looking for closest Match of + " + this + " in " + repo);
        MapachoArtifact lastMod = null;
        for (File f : repo.listFiles()) {
            MapachoArtifact ma = fromFile(application, f);
            if (isSameArtifactButDifferentVersion(ma)) {
                if (lastMod == null || lastMod.getFile().lastModified() < f.lastModified()) {
                    lastMod = ma;
                }
            }
        }

        System.out.println("Looking for closest Match of + " + this + " in " + repo + " =" + lastMod);

        return lastMod;
    }

    public boolean matches() {
        JRE jre = application.getJre();
        String defaultLocale = Locale.getDefault().toString();

        return matches(jre.getOs(), os)
                && matches(defaultLocale, locale)
                && matches(jre.getArch(), arch);
    }

    @Override
    public String toString() {
        String app = application != null ? application.getDisplayName() + "v" + application.getDisplayVer() : "<not_set>";
        return "MapachoArtifact{" + "application=" + app + ", name=" + name + ", version=" + version + ", file=" + file + '}';
    }

}
