/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.common.runtime;

import java.io.File;
import java.io.Serializable;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import uno.anahata.mapacho.common.os.OSUtils;
import static org.apache.commons.lang3.StringUtils.*;

/**
 *
 * @author pablo
 */
@Data
public class JRE implements Serializable {
    /**
     * E.g. 8u60
     */
    private String ver;

    /**
     * E.g. b27 as in 8u60-b27
     */
    private String build = "";

    /**
     * Required for Java 8 u121 onwards. e.g. e9e7ea248e2c4826b92b3f075a80e441. This is the part of the download URL
     * like:
     * e9e7ea248e2c4826b92b3f075a80e441.
     */
    private String hash = "";

    /**
     * The Operating system
     */
    private String os = "";

    /**
     * x86 or x64 if 32 bits needs to be forced on 64 bit platforms.
     */
    private String arch = "";
    
    /**
     * JRE location, resolved at runtime.
     */
    private transient File javaHome;
    
    public static JRE runningJRE() {
        JRE jre = new JRE();
        jre.setOs(OSUtils.getOsName());
        jre.setArch(System.getProperty("sun.arch.data.model"));
        jre.setJavaHome(new File(System.getProperty("java.home")));
        return jre;
    }

    public String getArch() {
        return arch;
    }

    public boolean is64bit() {
        return getArch().contains("64");
    }

    public String getOsSimple() {
        if (OSUtils.isMac(os)) {
            return "macosx";
        } else if (OSUtils.isWindows(os)) {
            return "windows";
        } else if (OSUtils.isLinux(os)) {
            return "linux";
        } else {
            return os.toLowerCase();
        }
    }

    private String getArchSimple() {
        return is64bit() ? "x64" : "i586";
    }

    public String getEncodedName() {
        String simpleArch = getArchSimple();
        String name = "jre-" + ver + "-" + getOsSimple() + "-" + simpleArch;
        return name;
    }

    public File getLocation(File repo) {
        File location = new File(repo, getEncodedName());
        return location;
    }
    
    public void calculateJavaHome(File mapachoRepo) {
        File location = new File(mapachoRepo, getEncodedName());
        setJavaHome(location);
    }

    public File getJavaExecutable() {
        return getJavaExecutable(getJavaHome());
    }
    
    public static File getJavaExecutable(File javaHome)  {        
        String preffix = OSUtils.isMac() ? "Contents" + File.separator + "Home" + File.separator : "";
        preffix = preffix + "bin" + File.separator;
        
        String suffix = OSUtils.isWindows() ? ".exe" : "";
        return new File(javaHome, preffix + "java" + suffix);
    }

    @Deprecated
    public String getOracleWebsiteDownloadURL() {
        String os = getOsSimple();
        String arch = is64bit() ? "x64" : "i586";
        String h = "";
        if (!isBlank(hash)) {
            h = "/" + hash;
        }
        String urlString;
        // TODO: A quick tweak if the project requires a fixed jre download version. Need to find a better approach. Eg: url_p.anahata.uno_8u121
        if (!isBlank(hash) && hash.startsWith("url-")) {
            String host = hash.split("-")[1];
            String jver = hash.split("-")[2];
            urlString = String.format("http://%s/jre-%s-%s-%s.tar.gz", host, jver, os, arch);
        } else {
            urlString = "http://download.oracle.com/otn-pub/java/jdk/" + getVer() + "-b" + getBuild() + h + "/jre-" + getVer() + "-" + os + "-" + arch + ".tar.gz";
        }
        return urlString;
    }

    public boolean matchesOS() {
        return System.getProperty("os.name").toLowerCase().startsWith(os.toLowerCase());
    }

    @Override
    public String toString() {
        return "JRE{" + "ver=" + ver + ", build=" + build + ", hash=" + hash + ", os=" + os + ", arch=" + arch + '}' + " downloadURL=" + getOracleWebsiteDownloadURL();
    }
    
    public static void main(String[] args) {
        JRE jre = JRE.runningJRE();
        
        System.out.println("f=" + jre.getJavaHome() + " " + jre.getJavaHome().exists());
    }

}
