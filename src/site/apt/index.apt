

  Mapacho is an open source Java Web Start framework for packing, deploying, launching and upgrading Desktop Java Applications. 

  Mapacho is based on Maven, the JNLP and Servlet APIs, a number of jar compression tools and a Java 8 JavaFX preloader / launcher. 

  The intention behind Mapacho is to allow users to deploy and launch the application with the latest version of Java over either 32 or 64 bit Java Web Start runtimes using a minimal jar with as simple as possible logic to reduce chances of JRE upgrade issues and making it as easy as possible to write workarounds for issues caused by Java Web Start bugs or JRE upgrades but then run the application in a "naked" JRE environment (without the JWS layer) and with a explicitely set JRE.

  Developers can define the version and architecture of the JRE to be used by the application (e.g. 8u60. x64) and any JVM flags supported by the JVM. The specified JRE is automatically downloaded from the Oracle website from the application server at runtime and streamed to the client. While the JRE is streamed to the client, a copy of it is written to the application servers temp directory so subsequent JRE requests are streamed from the application server itself. This normally makes the JRE download a bit faster than when downloading from the Oracle website, happens in parallel with the download of the application's JAR files.

  Mapacho uses the jardiff protocol to only transmit the portions of JARs that have changed from one version to another and uses pack200 and gz compression for jars as well as for jardiffs.

  Jar versioning is managed using Maven APIs.

Framework benefits

* Build / Packaging 

 * Faster build / packaging. Applications with 100 jars get packaged for JWS deployment in about 2 seconds. This is achived by:

  * Removing pack, unpack, repack and gz compression tasks from the build process

  * Reducing jar sign time by signing one 1Mb jar only per deployment descriptor (jnlp file) and caching it if no change has been detected.

  * Multi threaded packaging (resolving dependencies, packaging).

 * Correct handling of -SNAPSHOT dependencies (Using Maven Apis and appending unique identifier to -SNAPSHOT dependencies in the local maven repo).

 * Multiple JNLP files can be generated via maven plugin using a single JNLP template and descriptor inheritance so one can define some base settings for all JNLPs on a base configuration and then have the specifics of each JNLP on a maven configuration element.

 * Deferring JRE packaging to the application server's runtime 

 * Jar signing with timestamping so the application runs after the code signing certificate has expired.

 * Packages the desktop application in a .war module with a built in Servlet to serve application's artifacts

* Deployment

 * Improved deployment performance by

  * Using jar-diff, pack200 and .gz compression 

  * Caching compressed jar and jar-diff artifacts on the application server's temp dir  

  * Users download the specified JRE from the application server itself. 

  * If the users request a JRE that is not present on the Application server's hard drive, it will be streamed from the Oracle website to the client PC and writing a copy of it to the server's hard drive for further requests.

  * Artifact and JRE compression during the streaming process so decompression times so users only wait for network download time without decompression overheads.

 * Allows users to keep their java plugin up to date without compromising the stability of the application. This is achieved by running minimal code on the java web start container and hence reducing the chances of issues caused by JRE upgrades or JWS container bugs or behavior differences with the plain JVM runtime (what developers normally use when developing).

* Application startup 

 * Faster startup times as it reduces jar signature verification and class loading times in general.

 * Reduces the chances of startup issues caused by JRE upgrades by running only one minimal jar with as simple as possible logic on the web start container 

 * A JavaFX application preloader and launcher with the following characteristics:

  * Preloader runs on the JWS container and the application on a pure JVM communicating with the preloader over TCP.  

  * Multiple progress bars indicating jar and JRE download progressess

  * Preloader API to allow the application to send messages to the preloader over localhost TCP connection.

  * Animated at any time of the application's startup process (even during the start() method of a JavaFX application or while the application's JavaFX thread is loading the initial Scene. 

  * Displays custom messages and progress as the application starts up

  * Hide / show preloader with a fade animation as indicated by the application

  * Preloader is 300 Kb in size and uses a single jar so it downloads and displays quickly  




