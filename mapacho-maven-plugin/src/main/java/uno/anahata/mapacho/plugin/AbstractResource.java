package uno.anahata.mapacho.plugin;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.graph.DependencyFilter;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResult;

/**
 * This class represents a &lt;jarResource&gt; configuration element from the
 * pom.xml file. It identifies an artifact that is to be processed by the plugin
 * for inclusion in the JNLP bundle.
 *
 * @author pablo
 */
@Getter
@Setter
public class AbstractResource {

    private String groupId;

    private String artifactId;

    private String version;

    private String classifier;

    private String type;
    
    private Artifact artifact;

    private List<Artifact> dependencies = new ArrayList();
    
    public static final String LMR_SNAPSHOT_DATEFORMAT = "yyyyMMdd.HHmmss";

    /**
     * Resolves the main artifact and its dependencies.
     *
     * @throws Exception
     */
    public synchronized void resolve(MapachoMojo resources) throws Exception {

        if (artifact != null) {
            System.out.println("Already resolved");
            return;
        }
        ArtifactRequest request = new ArtifactRequest();
        
        if (type == null) {
            type = "jar";
        }
        
        request.setArtifact(
                new DefaultArtifact(getGroupId(), getArtifactId(), getClassifier(), getType(), getVersion()));
        //adding all repos, otherwise it only searches in maven central and local
        request.setRepositories(resources.getRemoteRepos());
        
        ArtifactResult result = resources.getRepoSystem().resolveArtifact(resources.getRepoSession(), request);
        artifact = result.getArtifact();
        System.out.println("Resolved artifact " + request + " to "
                + result.getArtifact().getFile() + " from "
                + result.getRepository());

        CollectRequest colReq = new CollectRequest();
        colReq.setRoot(new Dependency(result.getArtifact(), "runtime"));
        //adding all repos, otherwise it only searches in maven central and local
        colReq.setRepositories(resources.getRemoteRepos());
        DependencyRequest dr = new DependencyRequest(colReq, new DependencyFilter() {
            public boolean accept(DependencyNode node, List<DependencyNode> parents) {
                return true;
            }
        });
        

        //dependencies.add(artifact);
        DependencyResult dependencyResult = resources.getRepoSystem().resolveDependencies(resources.getRepoSession(), dr);
        for (ArtifactResult ar : dependencyResult.getArtifactResults()) {
            dependencies.add(ar.getArtifact());
        }

    }

    

}
