/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.plugin;

import java.io.*;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.SerializationUtils;
import uno.anahata.mapacho.common.app.MapachoApplication;
import uno.anahata.mapacho.common.app.MapachoArtifact;

/**
 *
 * @author pablo
 */
public class Packager {

    private final JnlpFile jnlpFile;

    @Getter
    private MapachoApplication app;
    
    @Getter
    private final MapachoArtifact launcher;
    
    @Getter
    private MapachoArtifact preloader;

    /**
     *
     * @param file
     * @throws Exception
     */
    public Packager(JnlpFile file) throws Exception {
        this.jnlpFile = file;
        
        installMapachoApplicationDescriptor();
        launcher = file.getResources().packageLauncher(file.getMetaInfApplicationName());
        //preloader = file.getResources().packagePreloader(file.getMetaInfApplicationName());
        file.getResources().packageLib2Artifacts(app);

    }
    
    private void installMapachoApplicationDescriptor() throws Exception {

        app = new MapachoApplication();
        app.setSplash(jnlpFile.getSplash());
        app.setPreloaderClass(jnlpFile.getPreloaderClass());
        app.setDisplayName(jnlpFile.getDisplayName());
        if (app.getDisplayName() == null) {
            app.setDisplayName(jnlpFile.getMetaInfApplicationName());
        }
        app.setDisplayVer(jnlpFile.getDisplayVersion());

        app.setJre(jnlpFile.getJre());

        app.setJvmArgs(jnlpFile.getJvmArgs());
        app.setMainClass(jnlpFile.getMainClass());
        app.setArguments(jnlpFile.getArguments());

        for (JarResource jr : jnlpFile.getJarResources()) {
            jr.addAllDependenciesToClassPath(app);
        }

        System.out.println(app);

        byte[] barr = SerializationUtils.serialize(app);
        File targetInWebModule = new File(
                jnlpFile.getResources().getMapachoDescriptorsDir() + File.separator + jnlpFile.getSerializedDescriptorFileName());
        FileUtils.writeByteArrayToFile(targetInWebModule, barr);
        System.out.println("Installing descriptor in web module" + targetInWebModule);

        //putInJar(new ByteArrayInputStream(barr), jar, "/" + jnlpFile.getSerializedDescriptorFileName());
        
        //System.out.println("Descriptor installed in jar, copying to " + Deployer.getDevAppSer());
        //IOUtils.write(barr, new FileOutputStream(Deployer.getDevAppSer()));

        //app.addAllDependenciesToClassPath(jar);
    }

    

    

}
