/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.plugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import lombok.Data;
import static uno.anahata.mapacho.common.io.MapachoIOUtils.*;

/**
 *
 * @author pablo
 */
@Data
public class KeystoreJarResource extends JarResource {
    private String keystore;

    private File keystoreFile;

    @Override
    public void resolve(MapachoMojo resources) throws Exception {
        super.resolve(resources); //To change body of generated methods, choose Tools | Templates.
        File artifactFile = getArtifact().getFile();

        if (keystore == null) {
            keystoreFile = artifactFile;
        } else {
            System.out.println("<keystore> entry specified inside <keystoreResource> will assume keystoreResource is zip and will try to "
                    + " find " + keystore + " inside jar " + artifactFile);
            
            Path temp = Files.createTempFile(keystore, artifactFile.getName());            
            Files.delete(temp);

            try (FileSystem zipfs = getZipFileSystem(artifactFile)) {
                /* Get the Path inside ZIP File to delete the ZIP Entry */
                Path pathInJar = zipfs.getPath(keystore);
                /* Execute Delete */
                if (!Files.exists(pathInJar)) {
                    throw new IOException("Couldn't find " + keystore + " in " + artifactFile);
                }

                System.out.println("Copying " + keystore + " in " + pathInJar + " to " + temp);
                Files.copy(pathInJar, temp);
            }
            
            temp.toFile().deleteOnExit();
            
            keystoreFile = temp.toFile();
            System.out.println("Setting lastmod " + temp + " to " + artifactFile.lastModified());
            //taking lastmodified from jar
            keystoreFile.setLastModified(artifactFile.lastModified());
        }
        
        resources.getSign().setKeystore(keystoreFile.toString());

    }

}
