package uno.anahata.mapacho.plugin;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.graph.DependencyFilter;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResult;
import uno.anahata.mapacho.common.app.MapachoApplication;
import uno.anahata.mapacho.common.app.MapachoArtifact;

/**
 * This class represents a &lt;jarResource&gt; configuration element from the
 * pom.xml file. It identifies an artifact that is to be processed by the plugin
 * for inclusion in the JNLP bundle.
 *
 * @author pablo
 */
@Getter
@Setter
public class JarResource extends AbstractResource {

    //Non maven classifiers
    private String os = "";

    private String arch = "";

    private String locale = "";

    private boolean nativelib = false;

    public MapachoArtifact toMapachoArtifact() {
        return toMapachoArtifact(null, getArtifact());
    }

    public void addAllDependenciesToClassPath(MapachoApplication parent) {

        for (Artifact a : getDependencies()) {            
            MapachoArtifact ma = toMapachoArtifact(parent, a);
            parent.getClasspath().add(ma);
        }
    }

    public MapachoArtifact toMapachoArtifact(MapachoApplication parent, Artifact a) {
        String name = a.getGroupId() + "-" + a.getArtifactId();
        String ver = a.getVersion();

        if (a.isSnapshot() && ver.contains("SNAPSHOT")) {
            String date = new SimpleDateFormat(LMR_SNAPSHOT_DATEFORMAT).format(a.getFile().lastModified());
            ver = ver.replace("SNAPSHOT", date + "-LMR");
        }

        MapachoArtifact ma = new MapachoArtifact(parent, name, os, arch, locale, ver, nativelib, a.getFile());
        return ma;
    }

}
