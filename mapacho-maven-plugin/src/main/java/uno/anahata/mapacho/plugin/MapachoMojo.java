package uno.anahata.mapacho.plugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Map.Entry;
import java.util.*;
import java.util.jar.Attributes.Name;
import java.util.jar.*;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.jarsigner.JarSigner;
import org.apache.maven.shared.jarsigner.JarSignerRequest;
import org.apache.maven.shared.jarsigner.JarSignerUtil;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;
import uno.anahata.mapacho.common.app.MapachoApplication;
import uno.anahata.mapacho.common.app.MapachoArtifact;
import uno.anahata.mapacho.common.art.Mapachito;
import static uno.anahata.mapacho.common.io.MapachoIOUtils.*;
import static uno.anahata.mapacho.plugin.AbstractResource.*;

/**
 *
 * @author pablo
 */
@Mojo(name = "mapacho", defaultPhase = LifecyclePhase.PREPARE_PACKAGE, requiresDependencyResolution = ResolutionScope.RUNTIME, aggregator = true)
//@Execute(goal = "mapacho", phase = LifecyclePhase.PROCESS_RESOURCES)
public class MapachoMojo extends AbstractMojo {
    
    

    @Getter
    public String buildTimestamp = new SimpleDateFormat(LMR_SNAPSHOT_DATEFORMAT).format(new Date());

    @Component
    @Getter
    private MavenProject project;

    /**
     * The collection of JnlpFile configuration elements. Each one represents a
     * JNLP file that is to be generated and deployed within the enclosing
     * project's WAR artifact. At least one JnlpFile must be specified.
     */
    @Parameter()
    @Getter
    private JnlpBase jnlpBase;

    /**
     * The collection of JnlpFile configuration elements. Each one represents a
     * JNLP file that is to be generated and deployed within the enclosing
     * project's WAR artifact. At least one JnlpFile must be specified.
     */
    @Parameter(required = true)
    private List<JnlpFile> jnlpFiles;

    /**
     * The collection of JnlpFile configuration elements. Each one represents a
     * JNLP file that is to be generated and deployed within the enclosing
     * project's WAR artifact. At least one JnlpFile must be specified.
     */
    @Parameter(required = true)
    @Getter
    private SignConfig sign;

    /**
     * The component to invoke jarsigner command.
     */
    @Component
    @Getter
    private JarSigner jarSigner;

    /**
     * The entry point to Aether, i.e. the component doing all the work.
     */
    @Component
    @Getter
    private RepositorySystem repoSystem;

    /**
     * The current repository/network configuration of Maven.
     */
    @Parameter(defaultValue = "${repositorySystemSession}", readonly = true)
    @Getter
    private RepositorySystemSession repoSession;

    /**
     * The project's remote repositories to use for the resolution of plugins
     * and their dependencies.
     */
    @Parameter(defaultValue = "${project.remotePluginRepositories}", readonly = true)
    @Getter
    private List<RemoteRepository> remoteRepos;

    @Parameter(defaultValue = "${mojoExecution}", readonly = true)
    @Getter
    private MojoExecution mojo;

    @Parameter(defaultValue = "${plugin}", readonly = true) // Maven 3 only
    @Getter
    private PluginDescriptor plugin;

    private final Map<String, MapachoArtifact> packagedLaunchersByApplicationName = new HashMap<>();

    private final Map<String, MapachoArtifact> packagedPreloadersByApplicationName = new HashMap<>();

    private final Set<File> packagedLib2Artifacts = new HashSet<>();

    @Getter
    private MapachoArtifact preloaderMapachoArtifact;

    public File getWorkingDirectory() {
        return new File(project.getBuild().getDirectory(), "mapacho");
    }

    public File getFinalDirectory() {
        File to = new File(getProject().getBuild().getDirectory(),
                getProject().getBuild().getFinalName() + File.separator + "mapacho");
        return to;
    }

    @Override
    public void execute() throws MojoExecutionException {

        System.out.println("*************** Mapacho Plugin Starts **********");

        long ts = System.currentTimeMillis();

        System.out.println("Init Code Signing");

        try {
            sign.init(this);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MojoExecutionException("Exception initializing SignConfig", e);
        }

        System.out.println("Code Signing initialized");

        System.out.println("mojo ver " + mojo.getVersion());
        System.out.println("Base Dir: " + project.getBasedir() + " context = " + super.getPluginContext());

        jnlpFiles.parallelStream().forEach((jnlpFile) -> {
            try {
                jnlpFile.execute(this);
            } catch (Exception e) {
                throw new RuntimeException("Excpetion generating jnlp", e);
            }
        });

        try {
            FileUtils.copyDirectory(getWorkingDirectory(), getFinalDirectory());
        } catch (Exception e) {
            throw new MojoExecutionException("Expcetion copying directory structure", e);
        }

        ts = System.currentTimeMillis() - ts;
        //http://www.text-image.com/convert/pic2ascii.cgi
        System.out.println(Mapachito.ASCII);
        System.out.println("total Mapacho time " + ts + " ms.");
        System.out.println("*************** Mapacho Plugin Ends **********");
    }

    public synchronized void packageLib2Artifacts(MapachoApplication app) throws Exception {
        app.getClasspath().parallelStream().forEach((ma) -> {

            File target = new File(getMapachoLibDir(), ma.getJarFileName());
            if (!packagedLib2Artifacts.contains(target)) {
                if (!target.exists()) {
                    try {
                        System.out.println("Copying " + ma.getFile().getName() + " to " + getMapachoLibDir());
                        FileUtils.copyFile(ma.getFile(), target);
                    } catch (IOException ex) {
                        ex.printStackTrace(System.err);
                        if (ex.getMessage().contains("Expected length")) {
                            //seen issue with commons-io
                            System.out.println("commons-io FileUtils.copyFile threw exception 'Expected length' regarding target file size: " + target.length() + " source file size: " + ma.getFile().length() + " ignoring.");
                        } else {
                            throw new RuntimeException(ex);
                        }
                    }
                    
                    try {
                        if (JarSignerUtil.isArchiveSigned(target)) {
                            System.out.println("Removing signature from " + target.getName());
                            JarSignerUtil.unsignArchive(target);
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
    }

    public synchronized MapachoArtifact packagePreloader(String applicationName) throws Exception {
        if (packagedPreloadersByApplicationName.containsKey(applicationName)) {
            return packagedPreloadersByApplicationName.get(applicationName);
        }
        JarResource mapacho_client_jar_without_deps = new JarResource();
        mapacho_client_jar_without_deps.setGroupId(getMojo().getGroupId());
        mapacho_client_jar_without_deps.setArtifactId("mapacho-client");
        //align versions
        mapacho_client_jar_without_deps.setVersion(getMojo().getVersion());
        //mapacho_client_jar_without_deps.setClassifier("jar");

        mapacho_client_jar_without_deps.resolve(this);

        preloaderMapachoArtifact = mapacho_client_jar_without_deps.toMapachoArtifact();

        packagePureJWSArtifact(preloaderMapachoArtifact, applicationName);

        packagedPreloadersByApplicationName.put(applicationName, preloaderMapachoArtifact);
        return preloaderMapachoArtifact;
    }

    public synchronized MapachoArtifact packageLauncher(@NonNull String applicationName) throws
            Exception {

        if (packagedLaunchersByApplicationName.containsKey(applicationName)) {
            return packagedLaunchersByApplicationName.get(applicationName);
        }

        JarResource mapachoJarWithDependencies = new JarResource();
        mapachoJarWithDependencies.setGroupId(getMojo().getGroupId());
        mapachoJarWithDependencies.setArtifactId("mapacho-client");
        //align versions
        mapachoJarWithDependencies.setVersion(getMojo().getVersion());
        mapachoJarWithDependencies.setClassifier("jar-with-dependencies");
        //mapachoJarWithDependencies.setMainClass(JavaFxDeployer.class.getName());
        mapachoJarWithDependencies.resolve(this);

        String mapachoVersion = mapachoJarWithDependencies.toMapachoArtifact().getVersion();

        String launcherName = applicationName.replaceAll("[^a-zA-Z0-9.-]", "-").toLowerCase();

        MapachoArtifact launcherMapachoArtifact = new MapachoArtifact();
        launcherMapachoArtifact.setName(launcherName + "-mapacho");
        launcherMapachoArtifact.setVersion(mapachoVersion + "-" + getSign().getUniqueString().hashCode());
        launcherMapachoArtifact.setFile(mapachoJarWithDependencies.getArtifact().getFile());

        String cacheKey = launcherMapachoArtifact.getJarFileName();

        File cachedFile = new File(getCacheDirectory(), cacheKey);

        if (false/*cachedFile.exists()*/) {
            System.out.println("Signed launcher already in cache " + cachedFile);
            File jarInLibDir = new File(getJnlpLibDir(), launcherMapachoArtifact.getJarFileName());
            FileUtils.copyFile(cachedFile, jarInLibDir);
            launcherMapachoArtifact.setFile(jarInLibDir);
        } else {
            System.out.println("Signed launcher not in cache: " + cachedFile);
            packagePureJWSArtifact(launcherMapachoArtifact, applicationName);
            FileUtils.copyFile(launcherMapachoArtifact.getFile(), cachedFile);
            System.out.println("Copied signed launcher to cache: " + cachedFile);
        }

        packagedLaunchersByApplicationName.put(applicationName, launcherMapachoArtifact);
        return launcherMapachoArtifact;

    }

    private void packagePureJWSArtifact(MapachoArtifact mapachoArtifact, String applicationName) throws Exception {

        File jarInLibDir = new File(getJnlpLibDir(), mapachoArtifact.getJarFileName());
        if (jarInLibDir.exists()) {
            System.out.println("Copying Pure JWS jar arleady present in " + jarInLibDir);
            return;
        } else {
            System.out.println("Copying Pure JWS jar to to " + jarInLibDir);
        }

        FileUtils.copyFile(mapachoArtifact.getFile(), jarInLibDir);
        mapachoArtifact.setFile(jarInLibDir);

        System.out.println(
                "Mapacho Jar With Deps copied to " + jarInLibDir + " size = " + FileUtils.byteCountToDisplaySize(
                        jarInLibDir.length()));

        processManifest(jarInLibDir, applicationName);

        putInJar(null, jarInLibDir, "META-INF/INDEX.LIST");

        File temp = new File(jarInLibDir.getParent(), jarInLibDir.getName() + ".pack");

        System.out.println("Launch jar before repack" + jarInLibDir + " size" + jarInLibDir.length());

        long ts = System.currentTimeMillis();

        System.out.println("Packing " + jarInLibDir + " to " + temp);

        try (FileOutputStream fos = new FileOutputStream(temp)) {
            Pack200.newPacker().pack(new JarFile(jarInLibDir), fos);
        }

        jarInLibDir.delete();

        System.out.println("Packed " + temp + " size=" + temp.length() + " unpacking...");
        try (JarOutputStream jos = new JarOutputStream(new FileOutputStream(jarInLibDir))) {
            Pack200.newUnpacker().unpack(temp, jos);
        }

        temp.delete();

        ts = System.currentTimeMillis() - ts;

        System.out.println(
                "repack took " + ts + "ms. Launch jar after repack" + jarInLibDir + " size=" + jarInLibDir.length());

        //CompressionUtils.packAndGz(targetLauncherJar, targetLauncherJar);
        //System.out.println("mapacho descriptor installed. signing launcher jar " + jarInLibDir);
        JarSignerRequest signRequest = getSign().createSignRequest(jarInLibDir,
                jarInLibDir);

        System.out.println("signing jar " + jarInLibDir);
        
        getSign().run(signRequest);
        
        System.out.println("verifying signed jar " + jarInLibDir);

        JarSignerRequest verifyRequest = getSign().createVerifyRequest(jarInLibDir);
        getSign().run(verifyRequest);
    }

    private void processManifest(File jar, String applicationName) throws IOException {
        HashMap entries = new HashMap();
        entries.put("Application-Name", applicationName);
        entries.put("Codebase", "*");
        entries.put("Permissions", "all-permissions");

        Manifest newMainfest = createManifest(jar, entries);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newMainfest.write(baos);
        putInJar(new ByteArrayInputStream(baos.toByteArray()), jar, "META-INF/MANIFEST.MF");
    }

    /**
     * Create the new manifest from the existing jar file and the new entries
     *
     * @param jar
     * @param manifestentries
     * @return Manifest
     * @throws MojoExecutionException
     */
    private static Manifest createManifest(File jar, Map<String, String> manifestentries) throws IOException {

        try (JarFile jarFile = new JarFile(jar)) {

            // read manifest from jar
            Manifest manifest = jarFile.getManifest();

            if (manifest == null || manifest.getMainAttributes().isEmpty()) {
                manifest = new Manifest();
                manifest.getMainAttributes().putValue(Name.MANIFEST_VERSION.toString(), "1.0");
            }

            if (manifest.getMainAttributes().containsKey(Name.CLASS_PATH)) {
                System.out.println("Removing " + Name.CLASS_PATH + " MANIFEST entry from " + jar);
                manifest.getMainAttributes().remove(Name.CLASS_PATH);
            }

//            manifest.getMainAttributes().remove(new Name("Export-Package"));            
//            manifest.getMainAttributes().remove(new Name("Import-Package"));            
//            for (Object toBeRemoved1 : toBeRemoved) {
//                manifest.getMainAttributes().remove(toBeRemoved1);
//            }
//            manifest.getMainAttributes().remove(new Name("Bundle-Name"));
//            manifest.getMainAttributes().remove(new Name("Bundle-Description"));
//            manifest.getMainAttributes().remove(new Name("Bundle-Description"));
            // add or overwrite entries
            Set<Entry<String, String>> entrySet = manifestentries.entrySet();
            for (Entry<String, String> entry : entrySet) {
                manifest.getMainAttributes().putValue(entry.getKey(), entry.getValue());
            }

            System.out.println("-----Removing entries with signatures (if any)-------");

            for (String key : new ArrayList<>(manifest.getEntries().keySet())) {
                Attributes atts = manifest.getEntries().get(key);
                for (Entry<Object, Object> entry : atts.entrySet()) {
                    if (entry.getKey().toString().toLowerCase().contains("digest")) {
                        Attributes ret = manifest.getEntries().remove(key);
                        System.out.println("Removed " + ret.keySet() + " " + ret.values());
                        break;
                    }
                }
            }

            System.out.println("Final Manifest for " + jar);

            System.out.println("------ Main attributes-----");
            for (Object o : manifest.getMainAttributes().keySet()) {
                System.out.println(o + " = " + manifest.getMainAttributes().get(o));
            }

            System.out.println("-----Entries-------");
            for (String o : manifest.getEntries().keySet()) {
                System.out.println(o + " = " + manifest.getEntries().get(o).values());
            }

            return manifest;
        }
    }

    public File getJnlpLibDir() {
        return new File(getWorkingDirectory(), "lib");
    }

    public File getMapachoLibDir() {
        return new File(getWorkingDirectory(), "lib2");
    }

    public File getMapachoDescriptorsDir() {
        return new File(getWorkingDirectory(), "descriptors");
    }

    private static File getCacheDirectory() {
        File temp = FileUtils.getTempDirectory();
        File jwstemp = new File(temp, "anahata-mapacho-cache");
        if (!jwstemp.exists()) {
            System.out.println("Creating temp directory " + jwstemp);
            jwstemp.mkdirs();
        }
        return jwstemp;
    }

}
