/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.plugin;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import uno.anahata.mapacho.common.runtime.JRE;
import static org.apache.commons.lang.StringUtils.*;
/**
 *
 * @author pablo
 */
@Getter
@Setter
public class JnlpBase {
    protected String displayName;
    
    protected String displayVersion;
    
    protected String metaInfApplicationName;
    
    protected String splash;
    
    protected String mainClass;
    
    protected String preloaderClass;
    
    protected JRE jre;
    
    protected String jvmArgs;
    
    protected List<String> arguments = new ArrayList();

    protected String templateFilename;
    
    protected List<JarResource> jarResources = new ArrayList<>();
    
    @Override
    public String toString() {
        return getClass().getName() + "{" + "displayName=" + displayName + ", jre=" + jre + ", jvmArgs=" + jvmArgs + ", arguments=" + arguments + ", templateFilename=" + templateFilename + ", jarResources=" + jarResources + '}';
    }
    
}
