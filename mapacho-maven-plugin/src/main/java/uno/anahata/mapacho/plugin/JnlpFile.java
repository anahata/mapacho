package uno.anahata.mapacho.plugin;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import java.io.File;
import java.io.Writer;
import java.util.Properties;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.project.MavenProject;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.codehaus.plexus.util.WriterFactory;
import uno.anahata.mapacho.client.JavaFxDeployer;
import uno.anahata.mapacho.client.MapachoDownloadServiceListener;
import uno.anahata.mapacho.client.MapachoPreloader;
import uno.anahata.mapacho.client.TcpSocketPreloaderProxy;
import uno.anahata.mapacho.common.app.MapachoArtifact;
import uno.anahata.mapacho.common.runtime.MapachoPropertyNames;
import static org.apache.commons.lang.StringUtils.*;

@Getter
@Setter
public class JnlpFile extends JnlpBase {

    private String outputFilename;

    private VelocityEngine engine = new VelocityEngine();

    private Template velocityTemplate;

    private MapachoMojo resources;

    private Packager packager;

    /**
     * Creates a new uninitialized {@code JnlpFile}.
     */
    public JnlpFile() {
        // do nothing
    }

    private void loadDefaults(JnlpBase defaults) {

        if (defaults != null) {

            if (displayName == null) {
                displayName = defaults.getDisplayName();
            }

            if (metaInfApplicationName == null) {
                metaInfApplicationName = defaults.getMetaInfApplicationName();
            }

            if (splash == null) {
                splash = defaults.getSplash();
            }

            if (mainClass == null) {
                mainClass = defaults.getMainClass();
            }

            if (preloaderClass == null) {
                preloaderClass = defaults.getPreloaderClass();
            }

            if (displayVersion == null) {
                displayVersion = defaults.getDisplayVersion();
            }

            if (displayVersion == null) {
                displayVersion = resources.getProject().getVersion();
            }

            if (jre == null) {
                jre = defaults.getJre();
            } else if (defaults.getJre() != null) {

                if (isBlank(jre.getVer())) {
                    jre.setVer(defaults.getJre().getVer());
                }

                if (isBlank(jre.getBuild())) {
                    jre.setBuild(defaults.getJre().getBuild());
                }

                if (isBlank(jre.getHash())) {
                    jre.setHash(defaults.getJre().getHash());
                }

                if (isBlank(jre.getArch())) {
                    jre.setArch(defaults.getJre().getArch());
                }
                
                if (isBlank(jre.getOs())) {
                    jre.setOs(defaults.getJre().getOs());
                }
            }

            if (jvmArgs == null) {
                jvmArgs = defaults.getJvmArgs();
            }

            arguments.addAll(defaults.getArguments());

            if (templateFilename == null) {
                templateFilename = defaults.getTemplateFilename();
            }

            jarResources.addAll(defaults.getJarResources());
        }

        if (splash == null) {
            splash = "/images/splash-screen.png";
        }

        if (preloaderClass == null) {
            preloaderClass = TcpSocketPreloaderProxy.class.getName();
        }

        if (metaInfApplicationName == null) {
            System.out.println("metaInfApplicationName was not set, assuming " + resources.getProject().getArtifactId());
            metaInfApplicationName = resources.getProject().getArtifactId();
        }

        if (displayName == null) {
            displayName = metaInfApplicationName;
        }

    }

    public void execute(MapachoMojo resources) throws Exception {

        this.resources = resources;

        loadDefaults(resources.getJnlpBase());

        for (JarResource jarResource : jarResources) {
            jarResource.resolve(resources);
        }

        packager = new Packager(this);
        generateJNLP();

    }

    public String getOutputFileName() {
        if (StringUtils.isBlank(outputFilename)) {
            return FilenameUtils.getBaseName(templateFilename) + ".jnlp";
        } else {
            return outputFilename;
        }
    }

    public String getSerializedDescriptorFileName() {
        return getDescriptorId() + ".ser";
    }

    public String getDescriptorId() {
        String outputFileName = getOutputFileName();
        return FilenameUtils.getBaseName(outputFileName);
    }

    private void generateJNLP() throws Exception {

        MavenProject project = resources.getProject();

        String source = project.getBasedir() + "/src/main/jnlp";

        Properties properties = new Properties();
        properties.setProperty(VelocityEngine.RUNTIME_LOG_LOGSYSTEM_CLASS,
                "org.apache.velocity.runtime.log.NullLogSystem");
        properties.setProperty("file.resource.loader.path", source);
        engine.init(properties);

        System.out.println("Loading template: " + templateFilename);

        velocityTemplate = engine.getTemplate(templateFilename, "UTF-8");
        VelocityContext context = createAndPopulateContext(project, packager.getLauncher(), packager.getPreloader());
        File jnlp = new File(resources.getWorkingDirectory(), getOutputFileName());

        System.out.println("Generating JNLP " + jnlp);

        try (Writer writer = WriterFactory.newWriter(jnlp, "UTF-8")) {
            velocityTemplate.merge(context, writer);
            writer.flush();
        } catch (Exception e) {
            throw new Exception(
                    "Could not generate the template " + velocityTemplate.getName() + ": " + e.getMessage(), e);
        }

    }

//    public File packageLauncher() {
//        
//    }
    /**
     * Creates a Velocity context and populates it with replacement values
     * for our pre-defined placeholders.
     *
     * @return Returns a velocity context with system and maven properties added
     */
    protected VelocityContext createAndPopulateContext(MavenProject mavenProject, MapachoArtifact launcher,
            MapachoArtifact preloader) {

        VelocityContext context = new VelocityContext();

        String deps = "\n      <!-- Mapacho generated Dependencies -->"
                + "\n\t" + launcher.getJnlpDependencyString("lib", true, null);
        //+ "\n\t" + preloader.getJnlpDependencyString("lib", false, "progress");

        String splashURL = "$$context" + packager.getApp().getSplash();;

        StringBuilder propsStringBuilder = new StringBuilder();

        propsStringBuilder.append("\n      <!-- Mapacho generated JNLP properties -->");
        
        //no more pack200
        appendJnlpProp(propsStringBuilder, "packEnabled", "false");
        appendJnlpProp(propsStringBuilder, "versionEnabled", "false");
        appendJnlpProp(propsStringBuilder, MapachoPropertyNames.DESCRIPTOR, getDescriptorId());
        appendJnlpProp(propsStringBuilder, MapachoPropertyNames.DESCRIPTOR_TIMESTAMP, resources.getBuildTimestamp());
        appendJnlpProp(propsStringBuilder, MapachoPropertyNames.CODEBASE, "$$codebase");
        appendJnlpProp(propsStringBuilder, MapachoPropertyNames.CONTEXT, "$$context");
        appendJnlpProp(propsStringBuilder, MapachoPropertyNames.SPLASH_SCREEN, splashURL);

        context.put("dependencies", deps);
        context.put("descriptor", getDescriptorId());
        context.put("properties", propsStringBuilder.toString());
        context.put("splash", splashURL);
        context.put("project", mavenProject.getModel());

        // aliases named after the JNLP file structure
        context.put("displayName", displayName);
        context.put("displayVersion", displayVersion);
        context.put("informationTitle", mavenProject.getModel().getName());
        context.put("informationDescription", mavenProject.getModel().getDescription());
        if (mavenProject.getModel().getOrganization() != null) {
            context.put("informationVendor", mavenProject.getModel().getOrganization().getName());
            context.put("informationHomepage", mavenProject.getModel().getOrganization().getUrl());
        }

        // explicit timestamps could go here
        context.put("mainClass", JavaFxDeployer.class.getName());
        context.put("progressClass", MapachoDownloadServiceListener.class.getName());
        context.put("preloaderClass", MapachoPreloader.class.getName());

        return context;
    }

    private static void appendJnlpProp(StringBuilder sb, String key, String val) {
        sb.append("\n      <property name=\"jnlp." + key + "\" value=\"").append(val).append("\" />");
    }

}
