/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.plugin;

import java.io.File;
import java.security.Provider;
import lombok.Getter;
import lombok.Setter;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.shared.jarsigner.JarSignerRequest;
import org.apache.maven.shared.jarsigner.JarSignerSignRequest;
import org.apache.maven.shared.jarsigner.JarSignerVerifyRequest;
import org.apache.maven.shared.utils.cli.CommandLineException;
import org.apache.maven.shared.utils.cli.javatool.JavaToolResult;

/**
 *
 * @author pablo
 */
@Getter
@Setter
public class SignConfig {

    private MapachoMojo resources;

    /**
     *
     */
    private File workDirectory;

    /**
     *
     */
    private boolean verbose;

    /**
     */
    private String keystore;

    /**
     * If the keystore is a maven artifact.
     */
    private KeystoreJarResource keystoreResource;

    /**
     */
    private String storetype;

    /**
     */
    private String storepass;

    /**
     */
    private String keypass;

    /**
     */
    private String alias;
    
    private String tsaLocation;

    public void init(MapachoMojo resources) throws Exception {
        this.resources = resources;

        if (keystoreResource != null) {
            keystoreResource.resolve(resources);
        }
    }

    public String getUniqueString() {
        File ksFile = keystoreResource != null ? keystoreResource.getArtifact().getFile() : new File(keystore);
        long keystoreLastMod = ksFile.lastModified();
        System.out.println("SignConfig.getUniqueString keystoreLastMod keystore file exists = " + ksFile.exists());
        System.out.println("SignConfig.getUniqueString keystoreLastMod = " + keystoreLastMod);
        String base = "sign_" + ksFile + "_" + keystoreLastMod + "_" + storetype + "_" + storepass.hashCode() + "_" + keypass.hashCode() + "_" + alias + "_" + tsaLocation + '}'; 
        System.out.println("SignConfig.getUniqueString = " + base);
        return base;
    }

    /**
     * Creates a jarsigner request to do a sign operation.
     *
     * @param jarToSign the location of the jar to sign
     * @param signedJar the optional location of the signed jar to produce (if not set, will use the original location)
     * @return the jarsigner request
     */
    public JarSignerRequest createSignRequest(File jarToSign, File signedJar) {
        JarSignerSignRequest request = new JarSignerSignRequest();
        request.setAlias(getAlias());
        request.setKeypass(getKeypass());
        request.setKeystore(getKeystore());
        //request.setSigfile(getSigfile());
        request.setStorepass(getStorepass());
        request.setStoretype(getStoretype());
        request.setWorkingDirectory(workDirectory);
        //request.setMaxMemory("1g");        
        request.setVerbose(true);
        request.setArchive(jarToSign);
        request.setSignedjar(signedJar);
        for (Provider p : java.security.Security.getProviders()) {
            System.out.println(" " + p.getName() + " " + p.getInfo() + " ");
        }
        //request.setArguments("-sigalg", "SHA256withRSA", "-tsapolicyid", "1.3.6.1.4.1.6449.1.2.1.9");
        request.setTsaLocation(getTsaLocation());
        return request;
    }

    /**
     * Creates a jarsigner request to do a verify operation.
     *
     * @param jarFile the location of the jar to sign
     * @return the jarsigner request
     */
    public JarSignerRequest createVerifyRequest(File jarFile) {
        JarSignerVerifyRequest request = new JarSignerVerifyRequest();
        request.setCerts(true);
        request.setWorkingDirectory(workDirectory);
        //request.setMaxMemory(getMaxMemory());
        request.setVerbose(false);
        request.setArchive(jarFile);
        return request;
    }

    public void run(JarSignerRequest request) throws Exception {

        long ts = System.currentTimeMillis();
        System.out.println("Signing jar with " + resources.getJarSigner());
        JavaToolResult result = resources.getJarSigner().execute(request);

        CommandLineException exception = result.getExecutionException();
        int exitCode = result.getExitCode();
        if (exception != null) {
            System.err.println("Exception signing jar");  
            exception.printStackTrace();
            
        }

        if (exitCode != 0) {
            
            if (request instanceof JarSignerSignRequest) {                
                System.out.println("Deleting signed jar " + ((JarSignerSignRequest)request).getSignedjar());
                ((JarSignerSignRequest)request).getSignedjar().delete();
            }
            
            throw new MojoExecutionException(
                    "Could not run " + request + " on  ar " + request.getArchive() + ", use -X to have detail of error");
        }

        

        ts = System.currentTimeMillis() - ts;

        System.out.println(
                request.getClass().getSimpleName() + " on " + request.getArchive().getName() + " took " + ts + " m.");

    }
}
