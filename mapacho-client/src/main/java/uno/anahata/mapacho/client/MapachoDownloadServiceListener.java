package uno.anahata.mapacho.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.SplashScreen;
import java.net.URL;
import javax.jnlp.DownloadServiceListener;
import javax.swing.*;
import lombok.Getter;

/**
 *
 * @author pablo
 */
public class MapachoDownloadServiceListener implements DownloadServiceListener {

    @Getter
    private static MapachoDownloadServiceListener instance;

    @Getter
    private JFrame frame;

    private JProgressBar progressBar = null;

    static {
        log("MapachoDownloadServiceListener class loaded. System splash screen: " + SplashScreen.getSplashScreen());

        Thread sdh = new Thread("Shutdown Hook") {
            @Override
            public void run() {
                Mapacho.log("MapachoDownloadServiceListener shutdownhook starts ");
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    Mapacho.log("MapachoDownloadServiceListener shutdownhook ex " + e);
                }
                Mapacho.log("MapachoDownloadServiceListener shutdownhook ends");
            }
        };

        Runtime.getRuntime().addShutdownHook(sdh);

    }

    public MapachoDownloadServiceListener() {
        log("JWS Instance Constructor:");
        instance = this;
        
        try {
            SwingUtilities.invokeLater(() -> createUI());
        } catch (Exception e) {
            e.printStackTrace();
            log("Error instantiating JWS instance " + e);
        }
    }

    private void createUI() {
        log("Creating UI");
        String osName = System.getProperty("os.name");
        if (!osName.contains("mac") && !osName.contains("darwin")) {
            try {
//                for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
//                    if ("Nimbus".equals(info.getName())) {
//                        log("Setting Nimbus");
//                        UIManager.setLookAndFeel(info.getClassName());
//                        log("Nimbus set");
//                        break;
//                    }
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        JPanel top = createComponents();
        log("UI Components created");
        frame = new JFrame(); // top level custom progress
        // indicator UI
        frame.getContentPane().add(top, BorderLayout.CENTER);
        //frame.setBounds(300, 300, 400, 300);

        //frame.setPreferredSize(new Dimension(800, 600));
        frame.setUndecorated(true);
        frame.setBackground(Color.WHITE);
        frame.pack();
        frame.setMinimumSize(new Dimension(290, 15));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        SwingUtilities.invokeLater(() -> {
            top.add(progressBar, BorderLayout.SOUTH);
            frame.pack();
        });
        
        

        //frame.setVisible(true);
    }

    private JPanel createComponents() {
        final JPanel top = new JPanel();
        top.setBackground(Color.WHITE);
        top.setLayout(new BorderLayout(0, 0));

        try {
            URL splashScreen = Mapacho.getSplashScreenURL();

            if (splashScreen != null) {

                try {
                    log("Splash screen url: " + splashScreen);
                    ImageIcon icon = new ImageIcon(splashScreen);
                    JLabel lbl = new JLabel(icon);
                    top.add(lbl, BorderLayout.NORTH);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                log("No splash Screen found, cannot display splash screen");
            }
        } catch (Exception e) {
            log("Could not load SplashScreen from malformed URL " + Mapacho.getSplashString());
        }

        progressBar = new JProgressBar(0, 100);
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(true);
        progressBar.setString("Launching");
        //top.add(progressBar, BorderLayout.SOUTH);
        return top;
    }

    public static void dispose() {
        log("disposing");
        if (instance != null) {
            instance.hide();
        }
    }

    public void updateMessage(String message) {
        SwingUtilities.invokeLater(() -> {
            log("Updating progress message to " + message);
            frame.setVisible(true);
            progressBar.setIndeterminate(true);
            progressBar.setString(message);
        });
    }

    private void hide() {
        SwingUtilities.invokeLater(() -> {
            if (frame != null) {
                log("hiding");
                frame.setVisible(false);
            } else {
                log("hiding as frame was null");
            }
        });
    }

//    public void show() {
//        SwingUtilities.invokeLater(() -> {
//            frame.setVisible(true);
//        });
//    }

    private void updateDownloadProgress(Integer overallPercent, String message, URL url, String version) {

        String file = url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
        version = version != null ? " v" + version : "";
        final String displayValue = message + " " + file + version;
        log("updateDownloadProgress(" + overallPercent + "," + displayValue + ")");

        SwingUtilities.invokeLater(() -> {

            if (overallPercent != null) {
                progressBar.setIndeterminate(false);
                progressBar.setValue(overallPercent);
//                if (overallPercent < 100) {
//                    frame.setVisible(true);
//                }
            }

            progressBar.setString(displayValue);
        });
    }

    @Override
    public void progress(URL url, String string, long l, long l1, int overallPercent) {
        updateDownloadProgress(overallPercent, "Downloading ", url, string);
    }

    @Override
    public void validating(URL url, String string, long l, long l1, int overallPercent) {
        updateDownloadProgress(overallPercent, "Validating ", url, string);
    }

    @Override
    public void upgradingArchive(URL url, String string, int i, int overallPercent) {
        updateDownloadProgress(overallPercent, "Upgrading ", url, string);
    }

    @Override
    public void downloadFailed(URL url, String string) {
        updateDownloadProgress(0, "Download failed ", url, string);
    }

    private static void log(String message) {
        Mapacho.log("[MapachoDownloadServiceListener]" + message);
    }

    public static void main(String[] args) throws Exception {
        //MapachoDownloadServiceListener.getInstance().updateMessage("hello");
        Thread.sleep(5000);
    }

}
