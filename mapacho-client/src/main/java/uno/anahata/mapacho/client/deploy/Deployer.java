/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.deploy;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.application.Preloader.PreloaderNotification;
import org.apache.commons.io.FileUtils;
import uno.anahata.mapacho.client.JavaFxDeployer;
import uno.anahata.mapacho.client.LoadingStage;
import uno.anahata.mapacho.client.Mapacho;
import uno.anahata.mapacho.client.exec.ExecUtils;
import uno.anahata.mapacho.client.io.*;
import uno.anahata.mapacho.common.app.MapachoApplication;
import uno.anahata.mapacho.common.app.MapachoArtifact;
import uno.anahata.mapacho.common.art.Mapachito;
import uno.anahata.mapacho.common.runtime.JRE;
import uno.anahata.mapacho.common.runtime.MapachoPropertyNames;
import static org.apache.commons.lang3.StringUtils.*;
import static uno.anahata.mapacho.client.LoadingStage.StateNotification.*;
import static uno.anahata.mapacho.client.Mapacho.*;

/**
 *
 * @author pablo
 */
public class Deployer extends Thread {

    private ExecutorService downloaderThreadPool = Executors.newFixedThreadPool(8);

    private MapachoApplication app;

    private ServerSocket preloaderSocket;

    private static final String PRELOADER_PORT_JVM_PROP_PREFFIX = "-D" + MapachoPropertyNames.PRELOADER_PORT + "=";

    public Deployer() {
        super("MapachoDeployer");
    }

    @Override
    public void run() {
        try {

            if (!reLaunch()) {
                deploy();
                launch();
            }

            Mapacho.log("************************** JVM launched ******************************");
            Mapacho.log(Mapachito.ASCII);
            Mapacho.log("************************** JVM launched ******************************");

            JavaFxDeployer.runAsap(() -> LoadingStage.getInstance().updateMessage("Awaiting Remote Preloader Connection"));

            Mapacho.log("Preparing to accept on preloader socket");

            try (Socket s = preloaderSocket.accept()) {
                JavaFxDeployer.runAsap(() -> LoadingStage.getInstance().updateMessage("Remote Preloader connected"));
                Mapacho.log("Got socket");
                try (ObjectInputStream ois = new ObjectInputStream(s.getInputStream())) {
                    Object obj;
                    while ((obj = ois.readObject()) != null) {
                        PreloaderNotification pn = (PreloaderNotification) obj;
                        Mapacho.log("[TCP] Got remote preloader notification: " + pn);
                        //always on handleApplicationNotification regardless.
                        JavaFxDeployer.runAsap(() -> LoadingStage.getInstance().handleApplicationNotification(pn));

                        if (obj == DISPOSE) {
                            Mapacho.log("[TCP] remote preloader notification was DISPOSE, closing Socket");
                            break;
                        }
                    }
                }
            }

            preloaderSocket.close();

            Mapacho.log("Deployer.run() closing ServerSocket");

        } catch (Exception e) {
            e.printStackTrace(System.out);
            Mapacho.log(e.toString());
        }

        Platform.exit();
    }

    public void deploy() throws Exception {

        JavaFxDeployer.runAsap(() -> LoadingStage.getInstance().updateMessage("Deploying"));

        long ts = System.currentTimeMillis();
        app = getDescriptor();
        app.setCodeBase(Mapacho.getCodeBase());

        JRE jre = app.getJre();
        if (jre == null) {
            jre = JRE.runningJRE();
            Mapacho.log("JRE not specified in descriptor " + jre + "using running jre at " + jre.getJavaHome());
            app.setJre(JRE.runningJRE());
        } else if (!jre.matchesOS()) {
            Mapacho.log(
                    "JRE specified in descriptor " + jre + " does not match operating system name " + System.getProperty(
                            "os.name"));
            throw new RuntimeException("JRE specified in descriptor "
                    + jre + " does not match operating system name " + System.getProperty("os.name"));
        } else {
            jre.setOs(System.getProperty("os.name"));

            if (isBlank(jre.getArch())) {
                jre.setArch(System.getProperty("os.arch"));
            }            
            jre.calculateJavaHome(Repo.JRE_DIR);
            Mapacho.log("Calculated Java Home in mapacho JRE Repo: " + jre.getJavaHome());
            
        }

        File splash = getSplashFile();

        List<AbstractDownloadTask> tasks = new ArrayList<>();

        if (splash != null && !splash.exists()) {
            tasks.add(new SplashScreenDownloadTask(splash));
        }

        Mapacho.log("Expected JRE location: " + jre.getJavaHome());
        if (!jre.getJavaHome().exists()) {//we'll have to download it
            Mapacho.log("Expected JRE location: " + jre.getJavaHome() + " not found, downloading from Mapacho Servlet");
            tasks.add(new MapachoServletJreDownloadTask(jre));
        }

        File jarRepo = getJarRepo();
        Mapacho.log("Jar Repo: " + jarRepo);
        for (MapachoArtifact arti : app.getMatchingClassPath()) {
            if (!arti.getFile(jarRepo).exists()) {
                tasks.add(new ArtifactDownloadTask(arti, jarRepo));
            }
        }
        for (MapachoArtifact arti : app.getMatchingLibPath()) {
            if (!arti.getLibDir(jarRepo).exists()) {
                tasks.add(new ArtifactDownloadTask(arti, jarRepo));
            }
        }

        for (AbstractDownloadTask task : tasks) {
            downloaderThreadPool.submit(task);
        }
        downloaderThreadPool.shutdown();
        downloaderThreadPool.awaitTermination(60, TimeUnit.MINUTES);
        Mapacho.log("All Deployment tasks finished. Commit Log:");

        for (AbstractDownloadTask adt : tasks) {
            Mapacho.log("[Commited]" + adt.getComitted());
            if (adt.getComitted() == null) {
                throw new RuntimeException("At least one download Tasks did not succeed. Aborting", adt.getDownloadException());
            }
        }
        Mapacho.log("All Deployment tasks finished");

    }

    private String getLibPath() throws Exception {
        StringBuilder sb = new StringBuilder();
        for (MapachoArtifact ma : app.getMatchingLibPath()) {
            if (sb.length() > 0) {
                sb.append(File.pathSeparator);
            }
            sb.append(ma.getLibDir(getJarRepo()));
        }
        return sb.toString();
    }

    private String getClassPath() throws Exception {
        StringBuilder sb = new StringBuilder();
        for (MapachoArtifact ma : app.getMatchingClassPath()) {
            if (sb.length() > 0) {
                sb.append(File.pathSeparator);
            }
            sb.append(ma.getFile(getJarRepo()));
        }
        return sb.toString();
    }

    public void launch() throws Exception {
        JavaFxDeployer.runAsap(() -> LoadingStage.getInstance().updateMessage("Launching"));

        File splash = getSplashFile();

        long ts = System.currentTimeMillis();

        File java = app.getJre().getJavaExecutable();

        List<String> cmdArray = new ArrayList<>();
        cmdArray.add(java.getAbsolutePath());
        cmdArray.add("-client");

        if (splash != null) {
            //cmdArray.add("-splash:" + splash.getAbsolutePath());
        }

        for (String s : app.getJvmArgs().split(" ")) {
            cmdArray.add(s);
        }

        for (String s : System.getProperties().stringPropertyNames()) {
            if (s.startsWith("jnlp.")) {
                String stripped = s.substring("jnlp.".length());
                cmdArray.add("-D" + stripped + "=" + System.getProperty(s));
            }
        }

        cmdArray.add("-Dmapacho.java.home=" + System.getProperty("java.home"));
        //cmdArray.add("-D" + MapachoPropertyNames.PRELOADER_PORT + "=" + preloaderSocket.getLocalPort());
        cmdArray.add("-Danahatautil.nonjws.application.url=" + Mapacho.getCodeBase() + "../");

        if (!isBlank(app.getPreloaderClass())) {
            cmdArray.add("-Djavafx.preloader=" + app.getPreloaderClass());
        }

//        if (splash != null) {
//            cmdArray.add("-D" + SPLASH_SCREEN + "=" + splash.getAbsolutePath());    
//        }
        if (getLibPath().length() > 0) {
            cmdArray.add("-Djava.library.path=" + getLibPath());
        }

        cmdArray.add("-cp");
        cmdArray.add(getClassPath());
        cmdArray.add(app.getMainClass());
        //adding startup arguments
        cmdArray.addAll(JavaFxDeployer.getStartupArguments());
        cmdArray.addAll(app.getArguments());

        Mapacho.log("Hiding main stage");
        //JavaFxDeployer.runAsap(() -> JavaFxDeployer.getInstance().getStage().hide());

        ts = System.currentTimeMillis() - ts;
        Mapacho.log("Launching method took " + ts + " until java process launching");

        File launcher = getCachedLauncherFile();

        if (!launcher.exists()) {
            File temp = new File(launcher.getParent(), launcher.getName() + ".tmp");
            FileUtils.writeLines(temp, cmdArray);
            boolean success = temp.renameTo(launcher);
            if (success) {
                Mapacho.log("Stored cached launcher at: " + launcher);
            } else {
                Mapacho.log("Could not store cached launcher at: " + launcher);
            }
        }

        initPreloaderSocket(cmdArray);

        Process p = ExecUtils.run(cmdArray.toArray(new String[0]));
        //Main.log("Launcher exitValue " + p.exitValue());
        Mapacho.log("Process launched. Calling Platform.exit");
    }

    public MapachoApplication getDescriptor() throws Exception {
        DescriptorDownloadTask ddt = new DescriptorDownloadTask(getDescriptorId());
        Future result = downloaderThreadPool.submit(ddt);
        result.get();
        MapachoApplication descriptor = ddt.getDescriptor();
        if (descriptor == null) {
            throw new IllegalStateException("Descriptor " + getDescriptorId() + " did not download correctly");
        } else {
            Mapacho.log("got descriptor " + descriptor);
        }
        return descriptor;

    }

    public static File getDevAppSer() throws Exception {
        return new File(System.getProperty("user.home") + File.separator + ".anahata", "dev-app.ser");
    }

    private boolean reLaunch() throws Exception {
        File launcher = getCachedLauncherFile();
        if (launcher.exists()) {
            JavaFxDeployer.runAsap(() -> LoadingStage.getInstance().updateMessage("Launching"));
            List<String> cmdArray = FileUtils.readLines(launcher, "UTF-8");
            initPreloaderSocket(cmdArray);
            ExecUtils.run(cmdArray.toArray(new String[0]));
            return true;
        }
        return false;
    }

    public static File getCachedLauncherFile() {
        String launcherName = Mapacho.getDescriptorId() + "__V" + Mapacho.getDescriptorTimestamp() + ".txt";
        File cachedLauncher = new File(getJarRepo(), launcherName);
        return cachedLauncher;
    }

    public File getSplashFile() {
        if (app.getSplash() != null) {
            String splashFileName = Mapacho.getDescriptorId() + "__V" + Mapacho.getDescriptorTimestamp() + app.getSplash().replace(
                    "/", "_");
            File cachedLauncher = new File(getJarRepo(), splashFileName);
            return cachedLauncher;
        } else {
            return null;
        }
    }

    public static File getJarRepo() {

        //File base = Repo.MAPACHO_DIR;
        URL codeBase = Mapacho.getCodeBase();
        String prot = codeBase.getProtocol();
        String host = codeBase.getHost();
        int port = codeBase.getPort();
        String path = codeBase.getPath().replace("/", "_");

        String s1 = prot + "_" + host + "_" + port + "_" + path;

        File ret = new File(Repo.MAPACHO_DIR, s1);
        ret.mkdirs();
        return ret;
    }

    /**
     * Initializes the socket for the tcp preloader and returns the JVM property
     * for the java process to be launched to know what port it should be
     * connecting to.
     *
     * @return
     * @throws IOException
     */
    private void initPreloaderSocket(List<String> cmds) throws IOException {
        preloaderSocket = new ServerSocket(0);
        String prop = PRELOADER_PORT_JVM_PROP_PREFFIX + preloaderSocket.getLocalPort();
        cmds.add(2, prop);

    }
}
