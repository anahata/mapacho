/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import lombok.Getter;
import org.apache.commons.lang3.SerializationUtils;
import uno.anahata.mapacho.client.Mapacho;
import uno.anahata.mapacho.common.app.MapachoApplication;

/**
 *
 * @author pablo
 */
public class DescriptorDownloadTask extends AbstractDownloadTask {
    
    @Getter
    private MapachoApplication descriptor;
    
    private final String descriptorFileName;
    
    public DescriptorDownloadTask(String descriptorId) throws Exception {
        super();
        this.descriptorFileName = descriptorId;
        String urlString = Mapacho.getCodeBase().toString();
        urlString += "descriptors/" + descriptorId + ".ser";
        super.url = new URL(urlString);
    }

    @Override
    protected void init(HttpURLConnection conn) throws Exception {
        //MapachoArtifact closestMatch = ma.getClosestMatch();
        //conn.addRequestProperty("encoding", "pack200-gzip");
        //conn.addRequestProperty("accept-encoding", "pack200-gzip");        
        updateTitle("Downloading " + descriptorFileName);
        Mapacho.log("Downloading " + descriptorFileName);
    }

    @Override
    protected void process(InputStream is, long realLength) throws Exception {
        Mapacho.log("Download completed for " + descriptorFileName);
        descriptor = (MapachoApplication) SerializationUtils.deserialize(is);
        //Mapacho.log("Got descriptor " + descriptor);
    }
    
    
    @Override
    protected File commit() throws Exception {
        return null;
    }

}
