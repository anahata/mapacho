/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.client;

import java.awt.SplashScreen;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import uno.anahata.mapacho.client.deploy.Deployer;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import javax.jnlp.ServiceManager;
import lombok.Getter;
import uno.anahata.mapacho.client.deploy.Repo;
import static uno.anahata.mapacho.common.runtime.MapachoPropertyNames.*;

/**
 * Central class to get key environment properties and minimalistic (System.out) logging.
 *
 * @author pablo
 */
public class Mapacho {
    private static final long STARTUP = System.currentTimeMillis();

    @Getter
    private static URL codeBase;

    @Getter
    private static URL context;

    @Getter
    private static boolean jws;
    
    public static PrintStream log;

    static {
        
        try {
            
            log = new PrintStream(new FileOutputStream(Repo.LOG_FILE));
            log("getting codebase, context, jws, splash screen= " + SplashScreen.getSplashScreen());
            jws = isJavaWebStart();
            codeBase = new URL(getCodeBaseString());
            context = new URL(getContextString());
        } catch (Exception e) {
            e.printStackTrace(System.out);
            throw new RuntimeException(e);
        }
        log("codebase=" + codeBase);
        log("context=" + context);
        log("jws=" + jws);
    }

    public static void log(String message) {
        long ts = System.currentTimeMillis() - STARTUP;
        String msg = 
                ts + " ms. [" + Thread.currentThread().getName() + "] " + message + " - " + DateFormat.getTimeInstance(DateFormat.LONG).format(new Date());
        System.out.println(msg);
        log.println(msg);
    }

    public static String getJnlpFile() throws Exception {
        return getDescriptorId() + ".jnlp";
    }

    public static String getDescriptorId() {
        return getProperty(DESCRIPTOR);
    }

    public static URL getSplashScreenURL() throws MalformedURLException{
        return new URL(getSplashString());
    }

    public static String getDescriptorTimestamp() {
        return getProperty(DESCRIPTOR_TIMESTAMP);
    }

    private static String getCodeBaseString() {
        return getProperty(CODEBASE);
    }

    public static String getSplashString() {
        return getProperty(SPLASH_SCREEN);
    }
    
    public static String getPreloaderPort() {
        return getProperty(PRELOADER_PORT);
    }

    public static String getContextString() {
        return getProperty(CONTEXT);
    }

    private static boolean isJavaWebStart() {
        
        try {
            Class.forName("javax.jnlp.ServiceManager");
            return ServiceManager.getServiceNames() != null;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    private static String getProperty(String prop) {
        if (jws) {
            prop = "jnlp." + prop;
        }
        return System.getProperty(prop);
    }

    public static void fireCountdown() {
        Thread t = new Thread() {
            @Override
            public void run() {
                log("Mapacho exiting in 60 seconds");
                try {
                    Thread.sleep(60000);
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
                log("System.exit(0)");
                System.exit(0);
            }
        };

        t.setName("MapachoCountDownThread");
        t.setDaemon(false);
        log("Starting countdown thread");
        t.start();
        log("Countdown thread started");
    }

//    public static void main(String[] args) throws Exception {
//        log("main(String[] args) splash = " + SplashScreen.getSplashScreen());
//        
//        if (SplashScreen.getSplashScreen() != null) {
//            SplashScreen.getSplashScreen().setImageURL(getSplashScreenURL());
//        }
//        
////        if (SplashScreen.getSplashScreen() != null) {
////            log("main(String[] args) " + SplashScreen.getSplashScreen().getImageURL());
////            log("main(String[] args) " + SplashScreen.getSplashScreen().isVisible());
////        }
//        
//        if (Deployer.reLaunch()) {
//            fireCountdown();
//        } else {
//            log("relaunch file not available, launching main app");
//            JavaFxDeployer.main(args);
//            log("main app exited");
//        }
//
//    }
}
