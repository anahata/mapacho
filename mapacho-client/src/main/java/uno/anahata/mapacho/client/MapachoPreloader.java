package uno.anahata.mapacho.client;

import java.awt.Rectangle;
import java.awt.SplashScreen;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.application.Preloader.ErrorNotification;
import javafx.application.Preloader.PreloaderNotification;
import javafx.application.Preloader.ProgressNotification;
import javafx.application.Preloader.StateChangeNotification;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author pablo
 */
public class MapachoPreloader extends Preloader {

    static {
        Mapacho.log("MapachoPreloader static bloc. Splash = " + SplashScreen.getSplashScreen());
    }

    @Getter
    private static MapachoPreloader instance;

    LoadingStage loadingStage;

    Image splashImage;

    public MapachoPreloader() {
        Mapacho.log("MapachoPreloader instantiated");
        instance = this;
    }

    @Override
    public void init() throws Exception {
        Mapacho.log("MapachoPreloader init()");
        splashImage = new Image(new File(Mapacho.getSplashString()).toURI().toURL().toExternalForm(), true);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Mapacho.log("MapachoPreloader entry start()");
        this.loadingStage = new LoadingStage(stage, splashImage);
        stage.show();
        Mapacho.log("MapachoPreloader exit start()");
        //doing runlater here only to allow for the stage to be visible.

        Platform.runLater(() -> {
            if (SplashScreen.getSplashScreen() != null && SplashScreen.getSplashScreen().isVisible()) {
                Mapacho.log("Closing native splash screen");
                SplashScreen.getSplashScreen().close();
            }
        });
    }

    @Override
    public boolean handleErrorNotification(ErrorNotification info) {
        return loadingStage.handleErrorNotification(info);
    }

    @Override
    public void handleApplicationNotification(PreloaderNotification info) {
        loadingStage.handleApplicationNotification(info);
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification info) {
        loadingStage.handleStateChangeNotification(info);
    }

    @Override
    public void handleProgressNotification(ProgressNotification info) {
        loadingStage.handleProgressNotification(info);
    }

}
