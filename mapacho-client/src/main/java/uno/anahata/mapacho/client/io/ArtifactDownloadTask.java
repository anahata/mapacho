/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import org.apache.commons.io.IOUtils;
import uno.anahata.mapacho.client.Mapacho;
import uno.anahata.mapacho.client.deploy.Repo;
import uno.anahata.mapacho.common.app.MapachoArtifact;
import uno.anahata.mapacho.common.jardiff.JarDiffPatcher;
import static uno.anahata.mapacho.common.http.HttpParameters.*;

/**
 *
 * @author pablo
 */
public class ArtifactDownloadTask extends AbstractDownloadTask {

    //public static final String PACK200_GZIP_ENCODING = "pack200-gzip";
    public static final String GZIP_ENCODING = "gzip";

    public static final String JAR_MIMETYPE = "application/x-java-archive";

    public static final String JARDIFF_MIMETYPE = "application/x-java-archive-diff";

    private MapachoArtifact requestedVersion;

    private MapachoArtifact currentVersion;

    private File jarRepo;

    public ArtifactDownloadTask(MapachoArtifact ma, File jarRepo) throws Exception {
        super();
        this.requestedVersion = ma;
        this.jarRepo = jarRepo;
        StringBuilder sb = new StringBuilder(ma.getApplication().getCodeBase().toString());
        sb.append("lib2/").append(ma.getName()).append(".jar?");
        
        
        sb.append(ARG_VERSION_ID);
        sb.append("=");
        sb.append(encode(ma.getVersion()));
        
        sb.append("&");
        
        sb.append(ARG_LOCALE);
        sb.append("=");
        sb.append(encode(ma.getLocale()));
        
        sb.append("&");
        
        sb.append(ARG_OS);
        sb.append("=");
        sb.append(encode(ma.getOs()));
        
        sb.append("&");
        
        sb.append(ARG_ARCH);
        sb.append("=");
        sb.append(encode(ma.getArch()));
        
        currentVersion = ma.getBestCurrentVersionMatch(jarRepo);
        
        if (currentVersion != null) {
            
            sb.append("&");
        
            sb.append(ARG_CURRENT_VERSION_ID);
            sb.append("=");
            sb.append(encode(currentVersion.getVersion()));
            
        }
        
        super.url = new URL(sb.toString());
    }
    
    /**
     * Returns the jar file name.
     * 
     * @return 
     */
    @Override
    protected String getDisplayResourceName() {
        return requestedVersion.getJarGzFileName();
    }

    @Override
    protected void init(HttpURLConnection conn) throws Exception {
        //MapachoArtifact closestMatch = ma.getBestCurrentVersionMatch();
        conn.addRequestProperty("encoding", "gzip");
        conn.addRequestProperty("accept-encoding", "gzip");

    }
    
    @Override
    protected void process(InputStream is, long realLength) throws Exception {        
        if (GZIP_ENCODING.equals(conn.getContentEncoding())) {
            try (GZIPInputStream gzipIs = new GZIPInputStream(is)) {
                //Main.log("Creating Pack200CompressorInputStream");
                
                    if (realLength != -1) {
                        try (ProgressInputStream pis = new ProgressInputStream(gzipIs, realLength, this)) {
                            doProcess(pis);
                        }
                    } else {
                        doProcess(gzipIs);
                    }
            }
        } else {
            doProcess(is);
        }
    }

    private void doProcess(InputStream is) throws Exception {
        download = makeTempDownloadTarget(false);
        try (FileOutputStream fos = new FileOutputStream(download)) {
            IOUtils.copy(is, fos);
        }

    }

    @Override
    protected File commit() throws Exception {

        File requestedVersionUncomitted;
        File requestedVersionUncomittedLibDir = null;

        File requestedVersionInRepo = requestedVersion.getFile(jarRepo);
        File requestedVersionInRepoLibDir = requestedVersion.getLibDir(jarRepo);

        if (conn.getContentType().equals(JARDIFF_MIMETYPE)) {

            Mapacho.log(
                    "Appying JarDiff " + download + " to upgrade from " + currentVersion.getVersion() + " to " + requestedVersion.getVersion());
            requestedVersionUncomitted = File.createTempFile(requestedVersion.getJarFileName(),
                    currentVersion.getJarFileName(), Repo.TEMP_DIR);

            updateTitle("Patching " + currentVersion.getName() + " to " + requestedVersion.getVersion());

            try (FileOutputStream fos = new FileOutputStream(requestedVersionUncomitted)) {
                new JarDiffPatcher().applyPatch((a) -> updateProgress(a, 100), currentVersion.getFile(jarRepo).getPath(),
                        download.getPath(), fos);
            }

            Mapacho.log("JarDiff applied. Will commit: " + requestedVersionUncomitted
                    + " exits=" + requestedVersionUncomitted.exists()
                    + " length=" + requestedVersionUncomitted.length());

        } else {
            requestedVersionUncomitted = download;
        }

        //Artifacts containing native libs need to be unzipped and extracted so the directory can be added
        //to java.library.path
        if (requestedVersion.isNativeLib()) {
            requestedVersionUncomittedLibDir = makeTempDownloadTarget(true);
            uno.anahata.mapacho.common.io.MapachoIOUtils.extractJarFile(requestedVersionUncomitted,
                    requestedVersionUncomittedLibDir);
        }

        //allow for a parallel download in another mapacho jvm to have downloaded the jar at the same time
        if (!requestedVersionInRepo.exists()) {
            boolean success = requestedVersionUncomitted.renameTo(requestedVersionInRepo);

            if (!success) {
                throw new IllegalStateException(
                        "Could not commit downloaded artifact from " + requestedVersionUncomitted + " to " + requestedVersionInRepo);
            }

            if (requestedVersion.isNativeLib()) {
                if (!requestedVersionInRepoLibDir.exists()) {
                    success = requestedVersionUncomittedLibDir.renameTo(requestedVersionInRepoLibDir);
                    if (!success) {
                        throw new IllegalStateException(
                                "Could not commit extracted lib dir from " + requestedVersionUncomittedLibDir + " to " + requestedVersionInRepoLibDir);
                    }
                }
            }

        }

        return requestedVersionInRepo;
    }

}
