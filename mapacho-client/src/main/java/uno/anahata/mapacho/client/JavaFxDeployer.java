/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client;

import uno.anahata.mapacho.client.deploy.Deployer;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import lombok.Getter;

/**
 *
 * @author pablo
 */
@Getter
public class JavaFxDeployer extends Application {

    static {
        Mapacho.log("JavaFXDeployer static");
        new Deployer().start();
        Mapacho.log("new Deployer().start()");
    }

    private LoadingStage scene;

    @Getter
    private static JavaFxDeployer instance;

    @Getter
    private static List<String> startupArs;

    private static boolean toolkitInitialized = false;

    private static List<Runnable> deferredFxThreadRunnables = new ArrayList<>();

    private static final Object initLock = new Object();

    public static List<String> getStartupArguments() {
        return getInstance().getParameters().getRaw();
    }

    /**
     * Runs the given javafx thread runnable as soon as possible.
     *
     * @param r the javafx runnable
     */
    public static void runAsap(Runnable r) {

        synchronized (initLock) {
            if (!toolkitInitialized) {
                Mapacho.log("Deferring runnable " + r);
                deferredFxThreadRunnables.add(r);
            } else if (Platform.isFxApplicationThread()) {
                r.run();
            } else {
                Platform.runLater(r);
            }
        }

    }

    @Override
    public void init() throws Exception {
        Mapacho.log("JavaFXDeployer init()");
        JavaFxDeployer.instance = this;
    }

    @Override
    public void start(final Stage stage) throws Exception {
        Mapacho.log("JavaFXDeployer start()");
        URL splash = Mapacho.getSplashScreenURL();
        Image splashImage = new Image(splash.toString(), true);
        scene = new LoadingStage(stage, splashImage);
        stage.show();

        Mapacho.log("toolkitInitialized");
        Platform.runLater(() -> {
            synchronized (initLock) {
                toolkitInitialized = true;
                for (Runnable r : deferredFxThreadRunnables) {
                    Mapacho.log("Running deferredRunnable " + r);
                    r.run();
                    Mapacho.log("deferredRunnable " + r + " completed");
                }
            }
        });

    }

    @Override
    public void stop() throws Exception {
        Mapacho.log("in stop() hiding stage and firing countdown:");
        if (scene != null) {
            if (scene.getStage() != null) {
                scene.getStage().hide();
                Mapacho.log("in stop() called stage.hide");
            }
        }
        Mapacho.fireCountdown();
        Mapacho.log("in stop() countdown fired");
    }

    public static void main(String[] args) {
        Mapacho.log("JavaFXDeployer main(String[]) args");
        launch(args);
        Mapacho.log("JavaFXDeployer main(String[]) args exited");
    }
}
