/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author pablo
 */
public class ProgressInputStream extends FilterInputStream {

    private long readCount;

    private long total;

    private ProgressMonitor progressMonitor;

    public ProgressInputStream(InputStream in, long total, ProgressMonitor pm) {
        super(in);
        this.total = total;
        this.progressMonitor = pm;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return progress(super.read(b, off, len));
    }

    @Override
    public int read(byte[] b) throws IOException {        
        return progress(super.read(b));
    }

    @Override
    public int read() throws IOException {
        int ret = super.read(); 
        progress(1);
        return ret;
    }

    private int progress(int count) {
        readCount = readCount + count;
        progressMonitor.progress(readCount, total);
        return count;
    }

}
