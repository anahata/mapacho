/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.client.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import org.apache.commons.io.IOUtils;
import uno.anahata.mapacho.client.Mapacho;
/**
 *
 * @author pablo
 */
public class SplashScreenDownloadTask extends AbstractDownloadTask{
    private File targetLocation;
    public SplashScreenDownloadTask(File comitted) throws Exception {
        this.targetLocation = comitted;
        url = Mapacho.getSplashScreenURL();
    }

    @Override
    protected void init(HttpURLConnection conn) throws Exception {
        //jws may have this cached.
        conn.setUseCaches(true);        
    }
    
    @Override
    protected void process(InputStream is, long realLength) throws Exception {
        download = makeTempDownloadTarget(false);
        try (FileOutputStream fos = new FileOutputStream(download)) {
            IOUtils.copy(is, fos);
        } 
    }

    @Override
    protected File commit() throws Exception {        
        if (!download.renameTo(targetLocation)) {
            Mapacho.log("Exception renaming splash image from " + download + " to " + comitted);
        }
        return targetLocation;
    }
    
}
