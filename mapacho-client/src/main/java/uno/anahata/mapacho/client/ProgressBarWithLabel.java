/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.client;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.StackPane;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 *
 * @author pablo
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class ProgressBarWithLabel extends StackPane {
    
    @Getter
    private final Label label = new Label();

    @Getter
    private final ProgressBar progressBar = new ProgressBar();
    
    private LoadingStage loadingStage;
    
    void init(LoadingStage stage) {
        loadingStage = stage;
        
        //label.setText(title);
        prefWidthProperty().bind(stage.getProgressBars().widthProperty());
        maxWidthProperty().bind(prefWidthProperty());
        minWidthProperty().bind(prefWidthProperty());
        managedProperty().bind(visibleProperty());
        
        progressBar.setVisible(true);
        label.setVisible(true);
        progressBar.prefWidthProperty().bind(prefWidthProperty());
        progressBar.maxWidthProperty().bind(prefWidthProperty());
        progressBar.minWidthProperty().bind(prefWidthProperty());                
        getChildren().add(progressBar);
        
        label.setStyle("-fx-font-size:8");
        label.setStyle("-fx-text-fill:black");
        
        getChildren().add(label);
        
        //add to the loading stage        
        stage.getProgressBars().getChildren().add(this);
        loadingStage.adjustStage();
        Mapacho.log("Added Bars :" + loadingStage.getProgressBars().getChildren().size());
    }
    
    public void dispose() {
                loadingStage.getProgressBars().getChildren().remove(this);
            loadingStage.adjustStage();
        
        Mapacho.log("Removed Bars :" + loadingStage.getProgressBars().getChildren().size());
    }
    
}
