/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.client.exec;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import uno.anahata.mapacho.client.Mapacho;
import static java.lang.System.*;

/**
 *
 * @author pablo
 */
public class ExecUtils {
    
    private static void debug(String[] cmds) {
        StringBuilder out = new StringBuilder();
        for (String cmd : cmds) {
            out.append(cmd);
            out.append(" ");
        }
        Mapacho.log("Executing " + out);
    }
    
    public static final Process runOrfail(String[] cmds, int timeoutSecs) throws Exception {
        
        Process p = run(cmds);
        
        p.waitFor(timeoutSecs, TimeUnit.SECONDS);
        if (p.exitValue() != 0) {
            throw new IllegalStateException("Executing " + Arrays.toString(cmds) + " did not exit in " + timeoutSecs + " seconds");
        }
        return p;
    }
    
    public static final Process run(String[] cmds) throws Exception {
        debug(cmds);
        Process p = Runtime.getRuntime().exec(cmds);
        new ProcessStreamHandler(p, true).start();
        new ProcessStreamHandler(p, false).start();
        //displayOutput(p);
        return p;
    }
    
}
