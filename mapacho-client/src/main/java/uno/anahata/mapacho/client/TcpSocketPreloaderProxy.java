/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.client;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import javafx.application.Preloader;
import javafx.stage.Stage;
import uno.anahata.mapacho.client.LoadingStage.StateNotification;

/**
 *
 * @author pablo
 */
public class TcpSocketPreloaderProxy extends Preloader {
    private Socket socket;

    private ObjectOutputStream oos;

    @Override
    public void init() {
        try {
            socket = new Socket("localhost", new Integer(Mapacho.getPreloaderPort()));
            oos = new ObjectOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

    }

    @Override
    public boolean handleErrorNotification(ErrorNotification info) {
        return false;
    }

    @Override
    public void handleApplicationNotification(PreloaderNotification info) {
        transmit(info);
        if (info == StateNotification.DISPOSE) {
            close();
        }
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification info) {
        //transmit(info);
    }

    @Override
    public void handleProgressNotification(ProgressNotification info) {
        //transmit(info);
    }

    private void transmit(PreloaderNotification info) {
        try {

            if (socket.isClosed()) {
                Mapacho.log("ERROR Socket is closed. Cannot transmit: " + info + " ");
            } else {
                Mapacho.log("Transmitting " + info);
                oos.writeObject(info);
                oos.flush();
            }
            

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

    }

    private void close() {
        try {
            Mapacho.log("Closing Preloader socket");
            socket.close();
            Mapacho.log("Preloader socket closed");
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //never show
    }

}
