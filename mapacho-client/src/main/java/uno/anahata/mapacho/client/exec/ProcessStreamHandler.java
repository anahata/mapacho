/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.client.exec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import uno.anahata.mapacho.client.Mapacho;

/**
 *
 * @author pablo
 */
public class ProcessStreamHandler extends Thread {
    private Process p;

    private boolean errorStream;

    public ProcessStreamHandler(Process p, boolean errorStream) {
        this.p = p;
        this.errorStream = errorStream;
        setDaemon(true);
        setName("PSH-" + p.hashCode() + (errorStream ? "-e" : ""));
    }

    private InputStream getStream() {
        return errorStream ? p.getErrorStream() : p.getInputStream();
    }

    @Override
    public void run() {
        try (InputStream is = getStream()) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
                String s;
                while ((s = br.readLine()) != null && !isInterrupted()) {
                    if (errorStream) {
                        s = "[E]" + s;
                    } else {
                        s = "[S]" + s;
                    }
                    Mapacho.log(s);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.out);
        }
    }
}
