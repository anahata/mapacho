/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.net.HttpURLConnection;
import uno.anahata.mapacho.common.runtime.JRE;

/**
 *
 * @author pablo
 */
public class OracleJreDownloadTask extends JreDownloadTask {
    
    public OracleJreDownloadTask(JRE jre) throws Exception {
        super(jre);
    }

    @Override
    protected void init(HttpURLConnection conn) throws Exception {
        conn.addRequestProperty("Cookie", "oraclelicense=accept-securebackup-cookie");
    }

    @Override
    protected String makeURL() {
        return jre.getOracleWebsiteDownloadURL();
    }
    
    
}
