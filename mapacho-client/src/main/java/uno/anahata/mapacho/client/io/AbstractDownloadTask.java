/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import javafx.application.Platform;
import javafx.concurrent.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import uno.anahata.mapacho.client.JavaFxDeployer;
import uno.anahata.mapacho.client.LoadingStage;
import uno.anahata.mapacho.client.Mapacho;
import uno.anahata.mapacho.client.ProgressBarWithLabel;
import uno.anahata.mapacho.client.deploy.Repo;
import uno.anahata.mapacho.common.http.HttpConnectionUtils;
import uno.anahata.mapacho.common.http.HttpHeaders;

/**
 *
 * @author pablo
 */
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractDownloadTask extends Task<File> implements ProgressMonitor {

    @Getter
    protected URL url;

    @Getter
    protected File download;

    @Getter
    protected Exception downloadException;

    @Getter
    protected File comitted;

    protected HttpURLConnection conn;

    protected ProgressBarWithLabel progressBar;

    protected abstract void process(InputStream is, long realLength) throws Exception;

    protected abstract File commit() throws Exception;

    protected void init(HttpURLConnection conn) throws Exception {
    }

    @Override
    protected void scheduled() {
        JavaFxDeployer.runAsap(() -> {
            progressBar = LoadingStage.getInstance().createProgressBar();
            progressBar.visibleProperty().bind(runningProperty());
            progressBar.getLabel().textProperty().bind(titleProperty());
            progressBar.getProgressBar().progressProperty().bind(progressProperty());
            Mapacho.log("Progress bar added for " + url);
        });

    }

    @Override
    protected File call() throws Exception {

        for (int i = 0; i < 3; i++) {
            try {
                return doCall();
            } catch (Exception ex) {
                if (downloadException == null) {
                    downloadException = ex;
                } else {
                    downloadException = new Exception("Download failed on attempt " + 1, ex);
                }
                ex.printStackTrace(System.out);
                updateTitle("Retrying");
            }
        }
        throw downloadException;
    }

    protected File doCall() throws Exception {
        updateProgress(-1, 1);
        conn = connect(url);
        updateTitle("Connected: " + getDisplayResourceName());
        String contentType = conn.getContentType();
        long contentLength = conn.getContentLengthLong();
        long realLength = conn.getHeaderFieldLong(HttpHeaders.HEADER_REAL_CONTENT_LENGTH, -1);
        if (realLength == -1 && contentLength == -1) {
            updateProgress(-1, 1);
        }

        updateTitle("Downloading: " + getDisplayResourceName());
        //conn.disconnect();

        File ret = null;
        try (InputStream is = conn.getInputStream()) {
            Mapacho.log("Input stream is: " + is);

            //byte[] buff = new byte[1024 * 4];
            if (contentLength != -1) {
                Mapacho.log("Wrapping in ProgressInputStream");
                try (ProgressInputStream fis = new ProgressInputStream(is, contentLength, this)) {
                    Mapacho.log("Processing ProgressInputStream");
                    process(fis, -1);
                }
            } else {
                process(is, realLength);
            }
        }

        updateTitle("Comitting : " + conn.getURL().getFile());
        comitted = commit();
        updateTitle("Completed: " + conn.getURL().getFile());

        Mapacho.log(this + " commited " + comitted);

        return comitted;

    }

    protected String getDisplayResourceName() {
        if (conn != null) {
            return conn.getURL().getFile();
        } else if (url != null) {
            return url.getFile();
        } else {
            return "";
        }
    }

    @Override
    public void progress(long cur, long total) {
        //Main.log("progress " + cur + " / " + total);
        updateProgress(cur, total);
    }

    @Override
    protected void failed() {
        super.failed(); 
        Mapacho.log(this + " failed! ");
        if (getException() != null) {
            getException().printStackTrace(Mapacho.log);
        }
    }
    
    

    @Override
    protected void done() {
        if (progressBar != null) {
            JavaFxDeployer.runAsap(() -> progressBar.dispose());
            Mapacho.log("Progress bar removed for " + url);
        }
    }

    protected File makeTempDownloadTarget(boolean dir) throws IOException {

        String fileName = url.getFile();
        String preffix = "download";

        if (fileName.contains("/")) {
            fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
        }

        if (fileName.contains("?")) {
            fileName = fileName.substring(0, fileName.indexOf("?"));
        }

        if (url.getQuery() != null) {
            preffix += "_" + url.getQuery().replace("&", "_").replace("=", "_");
        }

        File parent = Repo.TEMP_DIR;
        Mapacho.log("Preffix=" + preffix + " suffix=" + fileName + " parent=" + parent);
        File ret = File.createTempFile(preffix, fileName, parent);
        if (dir) {
            ret.delete();
            ret.mkdirs();
        }
        ret.deleteOnExit();
        Mapacho.log("Download target :" + ret);
        return ret;
    }

    private HttpURLConnection connect(URL url) throws Exception {
        updateTitle("Connecting to " + url);
        HttpURLConnection localConn = (HttpURLConnection)url.openConnection();
        localConn.setInstanceFollowRedirects(true);
        localConn.setAllowUserInteraction(false);
        localConn.setUseCaches(false);
        init(localConn);

        Mapacho.log("Content-type:" + localConn.getContentType());
        Mapacho.log("Content-encoding:" + localConn.getContentEncoding());
        Mapacho.log("Content-Length:" + localConn.getContentLengthLong());
        Mapacho.log(HttpHeaders.HEADER_REAL_CONTENT_LENGTH + ":" + localConn.getHeaderField(
                HttpHeaders.HEADER_REAL_CONTENT_LENGTH));
        Mapacho.log("Response-code:" + localConn.getResponseCode());
        Mapacho.log("Headers:" + localConn.getHeaderFields());

        String movedTo = HttpConnectionUtils.getMovedToLocation(localConn);
        if (movedTo != null) {
            localConn.disconnect();
            return connect(new URL(movedTo));
        }

        //got final URL
        this.url = url;
        return localConn;
    }

    protected static final String encode(String arg) throws UnsupportedEncodingException {
        if (arg == null) {
            arg = "";
        }
        return URLEncoder.encode(arg, "UTF-8");
    }

}
