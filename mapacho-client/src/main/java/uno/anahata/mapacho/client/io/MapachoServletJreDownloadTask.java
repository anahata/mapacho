/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.net.HttpURLConnection;
import uno.anahata.mapacho.client.Mapacho;
import uno.anahata.mapacho.common.runtime.JRE;
import static org.apache.commons.lang3.StringUtils.*;
import static uno.anahata.mapacho.common.http.HttpParameters.*;

/**
 *
 * @author pablo
 */
public class MapachoServletJreDownloadTask extends JreDownloadTask {
    
    public MapachoServletJreDownloadTask(JRE jre) throws Exception {
        super(jre);
    }

    @Override
    protected String makeURL() throws Exception {
        StringBuilder sb = new StringBuilder(Mapacho.getCodeBase().toString());
        sb.append("jre?");
        sb.append(ARG_VERSION_ID);
        sb.append("=");
        sb.append(encode(jre.getVer()));
        if (!isBlank(jre.getBuild())) {
            sb.append("_");
            sb.append(jre.getBuild());
        }
        //hash added since 8u121
        if (!isBlank(jre.getHash())) {
            sb.append("_");
            sb.append(jre.getHash());
        }
        sb.append("&");
        sb.append(ARG_OS);
        sb.append("=");
        sb.append(encode(jre.getOs().toLowerCase()));
        sb.append("&");
        sb.append(ARG_ARCH);
        sb.append("=");
        sb.append(encode(jre.getArch()));
        
        return sb.toString();
    }

    @Override
    protected void init(HttpURLConnection conn) throws Exception {
        
    }
    
}
