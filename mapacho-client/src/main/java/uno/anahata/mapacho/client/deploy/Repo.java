/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.deploy;

import java.io.File;
import uno.anahata.mapacho.common.app.MapachoApplication;

/**
 *
 * @author pablo
 */
public class Repo {
    public static final File ANAHATA_DIR = new File(System.getProperty("user.home"), ".anahata");
    public static final File MAPACHO_DIR = new File(ANAHATA_DIR, "mapacho");
    public static final File LOG_DIR = new File(MAPACHO_DIR, "log");
    public static final File LOG_FILE = new File(LOG_DIR, System.currentTimeMillis() + ".log.txt");        
    public static final File JRE_DIR = new File(MAPACHO_DIR, "jre");
    public static final File TEMP_DIR = new File(MAPACHO_DIR, "tmp");
    
    static {
        JRE_DIR.mkdirs();
        TEMP_DIR.mkdirs();
        LOG_DIR.mkdirs();
    }
}
