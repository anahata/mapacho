/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import org.apache.commons.lang3.Validate;
import uno.anahata.mapacho.client.Mapacho;
import uno.anahata.mapacho.client.deploy.Repo;
import uno.anahata.mapacho.client.exec.ExecUtils;
import uno.anahata.mapacho.common.os.OSUtils;
import uno.anahata.mapacho.common.runtime.JRE;

/**
 *
 * @author pablo
 */
public abstract class JreDownloadTask extends TarGzDownloadTask {
    protected JRE jre;
    
    public JreDownloadTask(JRE jre) throws Exception {
        super();
        this.jre = jre;
        super.url = new URL(makeURL());
        Mapacho.log("JRE download URL = " + url);
        //super.download = jre.getLocation(Repo.JRE_DIR);
    }
    
    protected abstract String makeURL() throws Exception;

    @Override
    protected File commit() throws Exception {
        File download = super.getDownload();
        File target = jre.getJavaHome();
        Mapacho.log("Comitting JRE from " + download + " to " + target);
        Validate.isTrue(download.listFiles().length == 1, "Download contains more than one entry");
        
        File source = download.listFiles()[0];
        Validate.isTrue(source.isDirectory(), "Download contains one entry that is not a directory");
        Mapacho.log("Moving " + source + " to " + target);
        boolean success = source.renameTo(target);
        if (!success) {
            throw new IllegalStateException("Could not move downloaded JRE from " + source + " to " + target);
        }
        
        if (OSUtils.isLinux() || OSUtils.isMac()) {
            File java = jre.getJavaExecutable();            
            Mapacho.log("java executable " + java + " canExecute:" + java.canExecute());
            if (!java.canExecute()) {
                Mapacho.log("making java executable " + java + " canExecute:" + java.canExecute());
                java.setExecutable(true, false);                
                Mapacho.log("after setExecutable " + java + " canExecute:" + java.canExecute());
            }
            
            if (!java.canExecute()) {
                String[] cmd = {"chmod", "+x", java.getAbsolutePath()};
                Mapacho.log("Trying chmod +x to make java executable: " + Arrays.asList(cmd));
                ExecUtils.runOrfail(cmd, 5);
            }
            
        } else {
            Mapacho.log("Not making java executable as os is neither linux or Mac. System.getProperty(\"os.name\") = " + System.getProperty("os.name"));
        }
        
        return target;
        
    }
    
}
