/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client;

import java.awt.Rectangle;
import java.io.Serializable;
import java.util.List;
import javafx.animation.FadeTransition;
import javafx.application.Preloader.ErrorNotification;
import javafx.application.Preloader.PreloaderNotification;
import javafx.application.Preloader.ProgressNotification;
import javafx.application.Preloader.StateChangeNotification;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.swing.JFrame;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author pablo
 */
@Getter
public class LoadingStage {

    static {
        Mapacho.log("Main static");
    }

    public static final long START = System.currentTimeMillis();

    private VBox root;

    private VBox progressBars;

    private TextArea log;

    private ImageView splashImageView;

    @Getter
    private Stage stage;

    @Getter
    private Scene scene;

    @Getter
    private static LoadingStage instance;

    @Getter
    private static List<String> startupArs;

    private Rectangle splashBounds;

    private ProgressBarWithLabel simpleProgressBar;

    public LoadingStage(final Stage stage, final Image splashImage) throws Exception {
        Mapacho.log("LoadingStage constructor, splashImage = " + splashImage + " stage=" + stage);

        this.stage = stage;
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setResizable(false);
        stage.setTitle("Launching");

        root = new VBox();
        root.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        root.setFillWidth(true);

        splashImageView = new ImageView(splashImage);

        splashImage.widthProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                calculateBounds();
            }
        });
        splashImage.heightProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                calculateBounds();
            }
        });

        //Main.log("Attempting " + splashScreenURL);
        root.getChildren().add(splashImageView);
        
        progressBars = new VBox();
        progressBars.prefWidthProperty().bind(splashImageView.fitWidthProperty());
        progressBars.maxWidthProperty().bind(splashImageView.fitWidthProperty());
        progressBars.minWidthProperty().bind(splashImageView.fitWidthProperty());
        progressBars.setBackground(
                new Background(new BackgroundFill(Color.TRANSPARENT, CornerRadii.EMPTY, Insets.EMPTY)));
        progressBars.setFillWidth(true);
        updateMessage("Initializing Mapacho");
        root.getChildren().add(progressBars);

        scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();

        root.prefHeightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(
                    ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //Main.log("Root changed so packing to height " + newValue);
                adjustStage();
            }
        });
        scene.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(
                    ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                //Main.log("Scene changed so packing " + newValue);
                adjustStage();
            }
        });

        instance = this;

    }

    private static final float CENTER_ON_SCREEN_X_FRACTION = 1.0f / 2;

    private static final float CENTER_ON_SCREEN_Y_FRACTION = 1.0f / 2;

    public void adjustStage() {
        //Main.log("packing scene height " + stage.getScene().getHeight());
        //Main.log("packing scene height " + root.prefHeightProperty().get() + " - " + root.getHeight());
        //Main.log("before sizing stage height" + stage.getHeight());

//        Mapacho.log(
//                "before stageToSceneSize() stage height:" + stage.getHeight() + " width" + stage.getWidth() + "x=" + stage.getX() + "y=" + stage.getHeight());

        stage.sizeToScene();

        if (splashBounds != null) {
            stage.setX(splashBounds.getX());
            stage.setY(splashBounds.getY());
            stage.setWidth(splashBounds.getWidth());
        } else {
            Mapacho.log("[WARN] No splash bounds during stageToSceneSize()");
        }

//        Mapacho.log(
//                "after stageToSceneSize() stage height:" + stage.getHeight() + " width" + stage.getWidth() + "x=" + stage.getX() + "y=" + stage.getHeight());
    }

    public ProgressBarWithLabel createProgressBar() {
        ProgressBarWithLabel ret = new ProgressBarWithLabel();
        ret.init(this);
        return ret;
    }

    public boolean handleErrorNotification(ErrorNotification info) {
        updateMessage(info.toString());
        return false;
    }

    public void handleApplicationNotification(PreloaderNotification info) {
        if (info instanceof MessageNotification) {
            updateMessage(info.toString());
        } else if (info instanceof StateNotification) {
            Mapacho.log(getClass().getName() + "-" + info);
            StateNotification state = (StateNotification)info;
            FadeTransition ft = new FadeTransition(Duration.seconds(1), scene.getRoot());
            ft.setFromValue(1);
            ft.setToValue(0);
            if (state == StateNotification.HIDE) {

            } else if (state == StateNotification.SHOW) {
                ft.setFromValue(0);
                ft.setToValue(1);
            } else if (state == StateNotification.DISPOSE) {
                updateMessage("Hiding Preloader stage on DISPOSE notification");
                ft.setOnFinished(e -> stage.hide());
            }
            ft.play();
        }

    }

    public void handleProgressNotification(ProgressNotification info) {
        updateMessage("Progress: " + info.getProgress());
    }

    public void handleStateChangeNotification(StateChangeNotification evt) {
        updateMessage(evt.getType().toString());
    }
 
    public void updateMessage(String m) {
        Mapacho.log(getClass().getName() + "-" + m);

        if (simpleProgressBar == null) {
            simpleProgressBar = createProgressBar();
            simpleProgressBar.getProgressBar().setProgress(-1);
        }

        
        simpleProgressBar.getLabel().setText(m);
        
    }

    @AllArgsConstructor
    public static class MessageNotification implements PreloaderNotification, Serializable {
        String message;

        @Override
        public String toString() {
            return message;
        }
    }

    public static enum StateNotification implements PreloaderNotification, Serializable {
        SHOW,
        HIDE,
        DISPOSE;
    }

    private void calculateBounds() {
        double realWidh = splashImageView.getImage().getWidth();
        double realHeight = splashImageView.getImage().getHeight();

        if (realWidh > 1 && realHeight > 1) {
//            if (Mapacho.isJws() && MapachoDownloadServiceListener.getInstance() != null) {
            if (false) {
                JFrame dwlFrame =MapachoDownloadServiceListener.getInstance().getFrame();
                splashBounds = new Rectangle(dwlFrame.getX(), dwlFrame.getY(), (int)realWidh,
                        (int)realHeight);

            } else {
                Rectangle2D bounds = Screen.getPrimary().getBounds();
                
                double centerX
                        = bounds.getMinX() + (bounds.getWidth() - realWidh)
                        * CENTER_ON_SCREEN_X_FRACTION;

                double centerY
                        = bounds.getMinY() + (bounds.getHeight() - realHeight)
                        * CENTER_ON_SCREEN_Y_FRACTION;

                splashBounds = new Rectangle((int)centerX, (int)centerY, (int)realWidh,
                        (int)realHeight);

                splashImageView.setFitWidth(realWidh);

            }

            Mapacho.log("Calculated bounds" + splashBounds);

            adjustStage();

        }

    }
}
