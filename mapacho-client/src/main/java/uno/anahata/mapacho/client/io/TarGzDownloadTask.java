/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.client.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;
import uno.anahata.mapacho.client.Mapacho;


/**
 *
 * @author pablo
 */
public abstract class TarGzDownloadTask extends AbstractDownloadTask {
    
    @Override
    protected void process(InputStream is, long realLength) throws Exception {
        //Main.log("I");

        Mapacho.log(getClass().getSimpleName() + " process() starts");

        updateTitle("Downloading");

        Mapacho.log(getClass().getSimpleName() + " making temp download target");

        download = makeTempDownloadTarget(true);
        
        Mapacho.log(getClass().getSimpleName() + " download target " + download);

        download.mkdirs();
        Mapacho.log(getClass().getSimpleName() + "download dir created : " + download);

        long realReadCount = 0;
        //Main.log("Creating GZipInputStream");
        try (GzipCompressorInputStream gzipIs = new GzipCompressorInputStream(is)) {
            //Main.log("Creating TarArchiveInputStream");
            try (TarArchiveInputStream tais = new TarArchiveInputStream(gzipIs)) {
                TarArchiveEntry entry = tais.getNextTarEntry();
                
                //File work = download;
                while (entry != null) {
                    Mapacho.log(getClass().getSimpleName() + " Entry: " + entry.getName());
                    File target = new File(download, entry.getName());
                        
                    if (entry.isDirectory()) {
                        
                        Mapacho.log(getClass().getSimpleName() + " creating directory for entry: " + target);
                        target.mkdirs();
                        Mapacho.log(getClass().getSimpleName() + "directory: " + target + " created. exists: " + target.exists());
                    } else {

                        //Main.log("Name= " + entry.getName());

                        //target.createNewFile();
                        byte[] content = new byte[(int)entry.getSize()];
                        tais.read(content);
                        
                        //create the necesary directories if they don
                        target.getParentFile().mkdirs();
                        
                        try (FileOutputStream fos = new FileOutputStream(target)) {
                            IOUtils.write(content, fos);
                            Mapacho.log("Written: " + target + " size=" + target.length());
                        }

                        updateTitle("Downloading :" + entry.getName());

                        if (realLength != -1) {
                            realReadCount += content.length;
                            updateProgress(realReadCount, realLength);
                        }

                    }
                    entry = tais.getNextTarEntry();
                }
            }
        }
        
    }

}
