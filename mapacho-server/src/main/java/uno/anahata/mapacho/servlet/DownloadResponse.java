/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.servlet;

import java.io.*;
import java.util.Date;
import java.util.zip.Deflater;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipParameters;
import org.apache.commons.io.IOUtils;
import uno.anahata.mapacho.common.http.HttpHeaders;
import uno.anahata.mapacho.servlet.io.CachingOutputStream;
import static uno.anahata.mapacho.common.http.HttpHeaders.*;

/**
 *
 * @author pablo
 */
@Getter
@Slf4j
public abstract class DownloadResponse {

    protected Date lastModified;

    @Setter
    protected String mimeType;

    protected long contentLength;

    /**
     * Uncompressed content length
     */
    protected long realContentLength;

    @Setter
    protected String contentEncoding;

    @Setter
    protected String versionId;

    /*
    @Setter
    @Getter
    protected boolean pack = false;
     */
    @Setter
    @Getter
    protected boolean gz = false;

    @Setter
    @Getter
    protected File cacheTarget;

    @Getter
    protected final DownloadRequest request;
    
    @Getter
    private OutputStream effectiveOs;

    public DownloadResponse(DownloadRequest request) {
        this.request = request;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
        if (lastModified != null) {
            lastModified = new Date((lastModified.getTime() / 1000) * 1000);
        }
    }

    private OutputStream getOutputStream(HttpServletResponse resp) throws IOException {
        effectiveOs = resp.getOutputStream();

        if (cacheTarget != null) {
            log.debug("{} wrapping in CachingOutputStream {}", request.getId(), this);
            effectiveOs = new CachingOutputStream(effectiveOs, cacheTarget);
        }

        if (gz) {
            log.debug("{} wrapping in GzipCompressorOutputStream {}", request.getId(), this);
            GzipParameters params = new GzipParameters();
            params.setCompressionLevel(Deflater.BEST_COMPRESSION);
            effectiveOs = new GzipCompressorOutputStream(effectiveOs, params);
            //effectiveOs = new DeflaterOutputStream(effectiveOs, new Deflater(Deflater.BEST_COMPRESSION), true);
            //effectiveOs = new BufferedOutputStream(effectiveOs);            
        }

        /*
        if (pack) {
            log.debug("Streaming: " + this + " will pack");
            effectiveOs = new Pack200CompressorOutputStream(effectiveOs, Pack200Strategy.IN_MEMORY);
        }
         */
        if (/*pack || */gz) {
            realContentLength = contentLength;
            contentLength = 0;
        }

        return effectiveOs;

    }

    public void sendResponse(DownloadRequest dr, HttpServletResponse resp) throws IOException, ServletException {
        try (InputStream is = getInputStream()) {

            try  {
                OutputStream os = getOutputStream(resp);
                if (is == null) {
                    log.error("{} Not Found {}", dr.getId(), this);
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                    return;
                }

                //set last modified header        
                //send the bloody thing
                if (dr.getIfModifiedSince() != null && !lastModified.after(dr.getIfModifiedSince())) {
                    log.debug("{} Not Modified {}", dr.getId(), this);
                    resp.sendError(HttpServletResponse.SC_NOT_MODIFIED);
                    return;
                }

                if (lastModified != null) {
                    log.debug("{} {}:{} {}", dr.getId(), HEADER_LASTMOD,lastModified.getTime(), this);
                    log.debug(HEADER_LASTMOD + ": " + lastModified.getTime());
                    resp.setDateHeader(HEADER_LASTMOD, lastModified.getTime());
                }

                if (versionId != null) {                    
                    log.debug("{} {}:{} {}", dr.getId(), HEADER_JNLP_VERSION,versionId, this);                    
                    resp.setHeader(HEADER_JNLP_VERSION, versionId);
                }

                if (contentLength != 0) {
                    //log.debug("Content-Length: " + contentLength);
                    log.debug("{} {}:{} {}", dr.getId(), "Content-Length",contentLength, this);
                    resp.setContentLengthLong(contentLength);
                }

                if (realContentLength != 0) {
                    log.debug("{} {}:{} {}", dr.getId(), HttpHeaders.HEADER_REAL_CONTENT_LENGTH,realContentLength, this);
                    resp.setHeader(HttpHeaders.HEADER_REAL_CONTENT_LENGTH, realContentLength + "");
                }

                if (contentEncoding != null) {
                    log.debug("{} {}:{} {}", dr.getId(), HEADER_CONTENT_ENCODING,contentEncoding, this);                    
                    resp.setHeader(HEADER_CONTENT_ENCODING, contentEncoding);
                }

                if (mimeType != null) {
                    //log.debug("Content-Type (MimeType): " + mimeType);
                    log.debug("{} {}:{} {}", dr.getId(), "Content-Type (MimeType):",mimeType, this);
                    resp.setContentType(mimeType);
                }

                log.debug("{} Streaming starts: " + is + " to " + os + " {}", request.getId(), this);

                IOUtils.copyLarge(is, os);
                os.flush();

                log.debug("{} Streaming ends: " + is + " to " + os + " {}", request.getId(), this);

            } catch (Exception e) {
                log.error(dr.getId() + " Exception in sendResponse 1" + dr, e);
                throw new ServletException(e);
            }
        } catch (Exception e) {
            log.error(dr.getId() + " Exception in sendResponse 2" + dr, e);
            throw new ServletException(e);
        }

    }

    protected abstract InputStream getInputStream() throws Exception;

    @Override
    public String toString() {
        return " " + ", lastModified=" + lastModified
                + ", mimeType=" + mimeType
                + ", contentLength=" + contentLength
                + ", realContentLength=" + realContentLength
                + ", contentEncoding=" + contentEncoding
                + ", versionId=" + versionId
                + ", cacheTarget=" + cacheTarget
                + ", gz=" + gz
                + "\n\t request=" + request;
    }

    //        if (dreq.isSupportsDeflate()) {            
//            try {
//                //file = AnahataCompressionUtils.compress(CompressionType.DEFLATE, file);
//                os = new DeflaterOutputStream(os, new Deflater(Deflater.BEST_COMPRESSION));
//                //resp.setHeader("encoding", "deflate");
//                resp.set
//            } catch (Exception e) {
//                log.warn("Could not compress file " + file, e);
//            }
//        }
}
