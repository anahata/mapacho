/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author pablo
 */
@Slf4j
public class FileDownloadResponse extends DownloadResponse {
    
    private File file;
    
    public FileDownloadResponse(DownloadRequest dreq) {
        this(dreq, dreq.getFile());
    }
    
    public FileDownloadResponse(DownloadRequest dreq, File file) {
        super(dreq);
        setFile(file);
    }
    
    private void setFile(File file) {
        this.file = file;
        if (file != null) {
            lastModified = new Date((file.lastModified() / 1000) * 1000);
            contentLength = file.length();
        }
    }

    @Override
    protected InputStream getInputStream() throws Exception{
        return file != null ? new FileInputStream(file) : null;
    }

    @Override
    public String toString() {
        return System.identityHashCode(super.request) + "|FileDownloadResponse" + "file=" + file + super.toString();
    }
    
    
}
