/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.servlet.io;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author pablo
 */
@Slf4j
public class StreamCache implements Closeable, AutoCloseable {
    @Getter
    private final FileOutputStream stream;

    private final File tempFile;

    private final File target;

    public StreamCache(File target) throws IOException {
        this.target = target;
        //target = new File(MapachoServlet.getCacheDirectory(), targetFileName);        
        tempFile = File.createTempFile(FilenameUtils.getBaseName(target.getName()),
                FilenameUtils.getExtension(target.getName()));
        System.out.println("Caching stream to " + tempFile + " will be moved to " + target + " on completion ");
        stream = new FileOutputStream(tempFile);
    }

    @Override
    public void close() throws IOException {
        if (stream != null) {
            stream.close();
            try {
                log.debug("Renaming " + tempFile + " to " + target);
                tempFile.renameTo(target);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Exception Renaming " + tempFile + " to " + target, e);

            }
        }

    }

}
