/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.servlet;

import java.io.File;
import java.util.HashMap;
import javax.servlet.ServletContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.mapacho.common.runtime.JRE;

/**
 *
 * @author pablo
 */
@RequiredArgsConstructor
@Slf4j
public class JreHandler {

    private final ServletContext servletContext;

    private HashMap<String, DownloadResponse> cache = new HashMap<>();

    public DownloadResponse getDownloadResponse(DownloadRequest dr) {
        

        JRE jre = dr.getJRE();
        log.info("JRE--> ::"+jre);
        File cachedFile = dr.getCachedJREFile();
        
        if (cachedFile.exists()) {
            log.info("Requested JRE in cache at: " + cachedFile);
            return new FileDownloadResponse(dr, cachedFile);
        } else {
            log.info("Requested JRE **NOT** in cache at: " + cachedFile);
            return new JREDownloadResponse(dr, jre);
        }
        
    }

}
