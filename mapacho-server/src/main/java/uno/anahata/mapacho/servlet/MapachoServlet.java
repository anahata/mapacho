/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.servlet;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import uno.anahata.mapacho.common.os.OSUtils;

/**
 *
 * @author pablo
 */
@WebServlet(name = "MapachoServlet", urlPatterns = "/mapacho/*", loadOnStartup = 1)
@Slf4j
public class MapachoServlet extends HttpServlet {

    private static final File MAPACHO_DIR;
    private static final File JRE_DIR;
    private static final File JAR_CACHE_DIR;
    

    static {
        File parent = OSUtils.isWindows() ? new File(System.getProperty("user.home")) : new File("/");
        File cacheDir = new File(parent, "mapacho");

        if (!cacheDir.exists()) {
            log.info("Creating cache directory in " + cacheDir);
            try {
                cacheDir.mkdirs();
            } catch (Exception e) {                
                log.error("Exception creating cache dir at user home, will try temp directory ", e);
                cacheDir = new File(FileUtils.getTempDirectory(), "mapacho");
                cacheDir.mkdirs();
                log.info("After creating cache dir in temp, exists:" + cacheDir.exists());
            }            
        }
        cacheDir.setWritable(true, false);
        log.info("Mapacho cache dir" + cacheDir);
        MAPACHO_DIR = cacheDir;
        MAPACHO_DIR.setWritable(true, false);
        
        JRE_DIR = new File(cacheDir, "jre");
        JRE_DIR.mkdirs();
        JRE_DIR.setWritable(true, false);
        log.info("Mapacho JRE dir" + JRE_DIR);
        
        JAR_CACHE_DIR = new File(cacheDir, "jar");
        JAR_CACHE_DIR.mkdirs();
        log.info("Mapacho jar and jardiff dir" + JAR_CACHE_DIR);
        
    }

    private JnlpFileHandler jnlpHandler;

    private JarHandler jarHandler;

    private JreHandler jreHandler;

    /**
     * Initialize servlet
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        jnlpHandler = new JnlpFileHandler(config.getServletContext());
        jarHandler = new JarHandler(config.getServletContext());
        jreHandler = new JreHandler(config.getServletContext());
    }

    /**
     * We handle get requests too - eventhough the spec. only requeres POST
     * requests
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String requestStr = request.getRequestURI();
        if (request.getQueryString() != null) {
            requestStr += "?" + request.getQueryString().trim();
        }

        //log.info("Request string {}", requestStr);        
        //log.info("User-Agent {}", request.getHeader("User-Agent"));

        DownloadRequest dreq = new DownloadRequest(request);
        log.info("{} New request {}", dreq.getId(), dreq);

        DownloadResponse dresp;
        if (dreq.isJnlp()) {
            log.info("{} New request is JNLP {}", dreq.getId(), dreq);
            dresp = jnlpHandler.getJnlpFileEx(dreq);
        } else if (dreq.isJar()) {
            log.info("{} New request is Jar {}", dreq.getId(), dreq);
            dresp = jarHandler.getDownloadResponse(dreq);
            if (dresp == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        } else if (dreq.isJRE()) {
            log.info("{} New request is JRE{}", dreq.getId(), dreq);
            dresp = jreHandler.getDownloadResponse(dreq);
        } else {
            log.info("{} New request is file{}", dreq.getId(), dreq);
            dresp = new FileDownloadResponse(dreq);
        }

        log.info("{} Got response {}", dreq.getId(), dresp);
        dresp.sendResponse(dreq, response);
        log.info("{} END response {}", dreq.getId(), dresp);
        dresp.getEffectiveOs().close();
        log.info("{} Closed effectiveOS {}", dreq.getId(), dresp);

    }

    public static File getJarCacheDir() {
        return JAR_CACHE_DIR;
    }

    public static File getJreDir() {
        return JRE_DIR;
    }
    

}
