/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.servlet;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
/**
 *
 * @author pablo
 */
public class ByteArrayDownloadResponse extends DownloadResponse {

    byte[] content;
    
    public ByteArrayDownloadResponse(DownloadRequest req, byte[] barr, String mimeType, Date lastModified) {
        super(req);        
        super.setLastModified(lastModified);
        super.mimeType = mimeType;        
        this.content = barr;
        this.contentLength = barr.length;
    }

    @Override
    protected InputStream getInputStream() throws Exception {
        return new ByteArrayInputStream(content);
    }

    @Override
    public String toString() {
        return System.identityHashCode(super.request) + "|ByteArrayDownloadResponse{" + "byteArrayLength=" + content.length + super.toString();
    }
    
    
}
