/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uno.anahata.mapacho.servlet;

import uno.anahata.mapacho.common.jardiff.JarDiff;
import java.io.*;
import java.util.HashMap;
import java.util.jar.JarOutputStream;
import java.util.zip.Deflater;
import java.util.zip.GZIPInputStream;
import javax.servlet.ServletContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipParameters;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import uno.anahata.mapacho.common.http.HttpHeaders;

/**
 *
 * @author pablo
 */
@RequiredArgsConstructor
@Slf4j
public class JarHandler {

    private final ServletContext servletContext;

    private HashMap<String, DownloadResponse> cache = new HashMap<>();

    public DownloadResponse getDownloadResponse(DownloadRequest dr) throws IOException {
        if (dr.getRequestedVersion() == null) {
            //not interested in these requests
            //something to think about here would be to check if there is any other version of the
            //jar in the cache, because if there isn't one, then we may as well return the entire thing??
            //would need to check if by doing so, jws goes into non versioned mode for subsequent updates
            log.debug("{} Ignoring non versioned jar request  {}", dr.getId(), dr);
            return null;
        }

        if (!dr.isJarExists()) {
            log.debug("{} Could not locate jar in .jar or .jar.gz  {}", dr.getId(), dr);

            return null;
        }

        log.debug("{} Processing versioned jar request  {}", dr.getId(), dr);

        boolean returnFullJar = isReturnFullJar(dr);

        File jarFile = dr.getRequestedVersionJarFile();
        File gzJarFile = dr.getRequestedVersionJarGzFile();

        File cacheJarFile = dr.getRequestedVersionCacheJarFile();
        File cacheJarGzFile = dr.getRequestedVersionCacheJarGzFile();

        log.debug("{} return full jar: {} {}", dr.getId(), returnFullJar, dr);

        log.debug("{} Requested version plain jar cache location: {} {}", dr.getId(), cacheJarFile, dr);

        if (returnFullJar) {

            if (!cacheJarFile.exists()) {
                //copy jar to cache if not there
                if (jarFile.exists()) {
                    log.info("{} Requested version plain jar not in cache, copying from {} to {} for future jardiffs", dr.getId(), jarFile,
                            cacheJarFile);
                    //should be done asynch really
                    FileUtils.copyFile(jarFile, cacheJarFile);
                    //copyAsynch(jarFile, cacheJarFile);
                } else if (gzJarFile != null && gzJarFile.exists()) {
                    //don´ know what is the point of this
                    log.info("{} !!!! Requested version plain jar not in cache but gz found, unzipping {} to {}",
                            gzJarFile,
                            cacheJarFile);
                    unzip(gzJarFile, cacheJarFile);
                } else {
                    log.error("{} Requested version not found in lib dirs, returning null {}", dr.getId(), dr);
                    return null;
                }
            }

            /*
            if (gzJarFile != null && gzJarFile.exists()) {
                log.info("!!! returning jar.gz in gzip encoding from web app lib directory: {}", gzJarFile);
                FileDownloadResponse ret = new FileDownloadResponse(dr, gzJarFile);
                ret.setContentEncoding(HttpHeaders.GZIP_ENCODING);
                ret.setMimeType(HttpHeaders.JAR_MIMETYPE);
                ret.setVersionId(dr.getRequestedVersion());
                return ret;
            } else 
             */
            if (!cacheJarGzFile.exists()) {
                zip(dr, jarFile, cacheJarGzFile);
            }

            FileDownloadResponse ret = new FileDownloadResponse(dr, cacheJarGzFile);
            ret.setContentEncoding(HttpHeaders.GZIP_ENCODING);
            ret.setMimeType(HttpHeaders.JAR_MIMETYPE);
            ret.setVersionId(dr.getRequestedVersion());
            log.info("{} Returning entire jar.gz from cache {}", dr.getId(), ret);
            return ret;

        }

        //ok, so both requested and current are available
        File diff = dr.getCacheDiffFile();
        File diffGz = dr.getCacheDiffGzFile();

        if (diffGz.exists()) {
            //diff done and compressed, return from cache
            FileDownloadResponse ret = new FileDownloadResponse(dr, diffGz);
            ret.setMimeType(HttpHeaders.JARDIFF_MIMETYPE);
            ret.setContentEncoding(HttpHeaders.GZIP_ENCODING);
            ret.setVersionId(dr.getRequestedVersion());
            log.info("{} JarDiff gz existed in cache, returning diff ", dr.getId(), ret);
            return ret;
        }

        if (!diff.exists()) {
            //have to make diff.
            if (!cacheJarFile.exists()) {

                log.info("{} JarDiff not in cache will create {}", dr.getId(), dr);

                if (jarFile != null && jarFile.exists()) {
                    log.info("{} Requested version jar not in cache, copying from /lib to cache dir {}", dr.getId(), dr);
                    FileUtils.copyFile(jarFile, cacheJarFile);
                } else if (gzJarFile != null && gzJarFile.exists()) {
                    log.info("{} !!!!Requested version jar not in cache but .jar.gzip found in lib, uncompressing from /lib to cache  to generate JarDiff {}", dr.getId(), dr);
                    unzip(gzJarFile, cacheJarFile);
                } else {
                    log.error("{} Cannot make jar diff as cannot locate requested version jar {}", dr.getId(), dr);
                }

            }

            log.info("{} creating jarddiff {}", dr.getId(), dr);
            makeJarDiff(dr, cacheJarFile, diff);
            log.info("{} jarddiff created {}", dr.getId(), dr);
        }

        //here we should probably check if the jardiff is bigger than the actual requested jar as the jardiff cannot be packed.
        FileDownloadResponse ret = new FileDownloadResponse(dr, diff);
        ret.setMimeType(HttpHeaders.JARDIFF_MIMETYPE);
        ret.setContentEncoding(HttpHeaders.GZIP_ENCODING);
        //ret.setPack(true);
        ret.setGz(true);
        ret.setCacheTarget(diffGz);
        ret.setVersionId(dr.getRequestedVersion());
        log.info("{} Returning newly created jardiff gzipping on the fly and storing jardiff.gz {}", dr.getId(), ret);
        return ret;

    }

    private static void makeJarDiff(DownloadRequest dr, File cacheJarFile, File diff) throws IOException {

        File diffTemp = File.createTempFile(diff.getName(), ".tmp");
        try (FileOutputStream fos = new FileOutputStream(diffTemp)) {
            JarDiff.createPatch(dr.getCurrentVersionCacheJarFile(), cacheJarFile, fos, true);
        }

        log.info("JarDiff created in temp file size {}", diffTemp.length());

        //packing the jardiff seems to cause SHA-256 problems, just returning unpacked
        FileUtils.deleteQuietly(diff);
        diffTemp.renameTo(diff);

        log.info("JarDiff (not packed) stored in cache {} size={}", diff.length());
    }

    private boolean isReturnFullJar(DownloadRequest dr) {
        boolean returnFullJar = false;

        if (dr.getPath().contains("lib/")) {
            log.info("{} Request is for lib/ jars, will return entire jar to reduce the chances of javaws bugs {}", dr.getId(), dr);
            returnFullJar = true;
        } else if (dr.getRequestedVersion().equals(dr.getCurrentVersion())) {
            log.info("{} Request current versionId is same as current version id, returning entire jar {}", dr.getId(), dr);
            returnFullJar = true;
        } else if (dr.getCurrentVersion() == null) {
            log.info("{} Request current versionId is null, returning entire jar {}", dr.getId(), dr);
            returnFullJar = true;
        } else {
            //curr version and requested version specified but not matching
            File currVersionJar = dr.getCurrentVersionCacheJarFile();
            if (!currVersionJar.exists()) {
                log.info("{} Current version jar not in cache, cannot diff so returning entire jar {}", dr.getId(), dr);
                returnFullJar = true;
            } else {
                returnFullJar = false;
                log.info("{} Current jar in cache, will not return entire jar {}", dr.getId(), dr);
            }
        }

        return returnFullJar;
    }
//
//    /**
//     * Performs an unpak operation asynchronously.
//     *
//     * @param jarPackGzSource the .pack.gz
//     * @param jarDest         the target jar file
//     */
//    private static void unpackAsynch(File jarPackGzSource, File jarDest) {
//        //todo use thread pool
//        new Thread(() -> {
//            try {
//                unpack(jarPackGzSource, jarDest);
//            } catch (Exception e) {
//                log.error("Exception in asynchronous unpack operation");
//            }
//
//        }).start();
//    }

//    /**
//     * Performs an copy operation asynchronously.
//     *
//     * @param source the .pack.gz
//     * @param dest   the target jar file
//     */
//    private static void copyAsynch(File source, File dest) {
//        //todo use thread pool
//        new Thread(() -> {
//            try {
//                log.debug("Copying " + source + " to" + dest);
//                
//                FileUtils.copyFile(source, dest);
//            } catch (Exception e) {
//                log.error("Exception in asynchronous copy operation");
//            }
//
//        }).start();
//    }
//    /**
//     * Unpacks a pack.gz into a jar
//     *
//     * @param jarPackGzSource
//     * @param jarDest
//     * @throws IOException
//     */
//    private static void unpack(File jarPackGzSource, File jarDest) throws IOException {
//        File temp = File.createTempFile(jarDest.getName(), ".tmp");
//        log.info("unpacking {} to {}", jarPackGzSource, jarDest);
//        //could be done in a separate thread
//        long ts = System.currentTimeMillis();
//        try (InputStream in = new BufferedInputStream(new GZIPInputStream(new FileInputStream(jarPackGzSource)));
//                JarOutputStream out = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(temp)))) {
//
//            Pack200.Unpacker unpacker = Pack200.newUnpacker();
//            //unpacker.properties().putAll( props );
//            unpacker.unpack(in, out);
//        }
//        FileUtils.deleteQuietly(jarDest);
//        temp.renameTo(jarDest);
//        ts = System.currentTimeMillis() - ts;
//        log.info("unpacking {} to {} took {} ms.", jarPackGzSource, jarDest, ts);
//
//    }
//    
    /**
     * Unpacks a gz into a jar
     *
     * @param jarPackGzSource
     * @param jarDest
     * @throws IOException
     */
    private static void unzip(File jarGzSource, File jarDest) throws IOException {
        File temp = File.createTempFile(jarDest.getName(), ".tmp");
        log.info("unzipping {} to {}", jarGzSource, jarDest);
        //could be done in a separate thread
        long ts = System.currentTimeMillis();
        try (InputStream in = new BufferedInputStream(new GZIPInputStream(new FileInputStream(jarGzSource))); JarOutputStream out = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(temp)))) {

            IOUtils.copy(in, out);
        }
        FileUtils.deleteQuietly(jarDest);
        temp.renameTo(jarDest);
        ts = System.currentTimeMillis() - ts;
        log.info("unzipping {} to {} took {} ms.", jarGzSource, jarDest, ts);

    }

    private void zip(DownloadRequest dr, File jarSource, File jarGzDest) throws IOException {
        File temp = File.createTempFile(jarGzDest.getName(), ".tmp");
        log.debug("{} gzipping {} to {} {}", jarSource, jarGzDest, dr);
        //could be done in a separate thread
        long ts = System.currentTimeMillis();
        GzipParameters params = new GzipParameters();
        params.setCompressionLevel(Deflater.BEST_COMPRESSION);
        try (InputStream is = new BufferedInputStream(new FileInputStream(jarSource)); GzipCompressorOutputStream out = new GzipCompressorOutputStream(new BufferedOutputStream(new FileOutputStream(temp)), params)) {
            IOUtils.copyLarge(is, out);
        }
        FileUtils.deleteQuietly(jarGzDest);
        temp.renameTo(jarGzDest);
        ts = System.currentTimeMillis() - ts;
        log.debug("{} gzipping {} to {} toom {} ms. {}", jarSource, jarGzDest, ts, dr);

    }

//    /**
//     * Packs a jar file into pack.gz
//     *
//     * @param source
//     * @param destination
//     * @throws IOException
//     */
//    private static void pack(File source, File destination) throws IOException {
//        long ts = System.currentTimeMillis();
//        log.debug("Packing \n\t{} size={} to \n\t{}", source, source.length(), destination);
//        File temp = File.createTempFile(destination.getName(), ".tmp");
//
//        try (JarFile jar = new JarFile(source, false); OutputStream out = new FileOutputStream(temp)) {
//
//            GZIPOutputStream gzipOut = new GZIPOutputStream(out) {
//                {
//                    def.setLevel(Deflater.BEST_COMPRESSION);
//                }
//            };
//            BufferedOutputStream bos = new BufferedOutputStream(gzipOut);
//
//            Pack200.Packer packer = Pack200.newPacker();
//            //packer.properties().putAll( props );
//            packer.pack(jar, bos);
//            bos.flush();
//            gzipOut.finish();
//        }
//        log.debug("pack operation to temp file finished size = {}, renaming {} to {}", temp.length(), temp, destination);
//        FileUtils.deleteQuietly(destination);
//        temp.renameTo(destination);
//        ts = System.currentTimeMillis() - ts;
//        log.debug("after renaming packed file {}, size is {} took {} ms.", destination, destination.length(), ts);
//    }
//    public static void main(String[] args) throws Exception {
//        File source = new File(
//                "/tmp/com.anahata-JobTracking-app-1.1.19-SNAPSHOT.20150429.164652-local-maven-repo-to-1.1.19-SNAPSHOT.20150429.165037-local-maven-repo.jardiff.pack.gz8049565465949587518.tmp");
//        File target = new File("/tmp/anahata-jws-cache/xxx.jardiff");
//        unpack(source, target);
//
//    }
}
