/*
 * Copyright 2016 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uno.anahata.mapacho.servlet;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import lombok.extern.slf4j.Slf4j;
import uno.anahata.mapacho.common.http.HttpConnectionUtils;
import uno.anahata.mapacho.common.http.HttpHeaders;
import uno.anahata.mapacho.common.runtime.JRE;


/**
 * Sends a JRE to the client, streaming it from the oracle website if not present on the caching file system.
 * 
 * @author pablo
 */
@Slf4j
public class JREDownloadResponse extends DownloadResponse {
    
    private URL url;
    private final JRE jre;
    
    public JREDownloadResponse(DownloadRequest req, JRE jre) {
        super(req);
        log.info("Info preparing download response of {}", jre);
        this.jre = jre;
        super.setCacheTarget(new File(MapachoServlet.getJreDir(), jre.getEncodedName() + ".tar.gz"));
        try {
            String urlStr = jre.getOracleWebsiteDownloadURL();
            log.info("JRE download URL from jre.getOracleWebsiteDownloadURL() method :: " + urlStr);
            url = new URL(urlStr);    
        } catch (Exception e) {
            log.error("Exception composing oracle download URL");
            throw new RuntimeException(e);
        }
        
    }

    @Override
    protected InputStream getInputStream() throws Exception {
        return connect(url).getInputStream();
    }
    
    private HttpURLConnection connect(URL url) throws Exception {
        log.info("Connecting to " + url);
        HttpURLConnection localConn = (HttpURLConnection)url.openConnection();
        localConn.setInstanceFollowRedirects(true);
        localConn.setAllowUserInteraction(false);
        localConn.setUseCaches(false);
        localConn.addRequestProperty("Cookie", "oraclelicense=accept-securebackup-cookie");

        System.out.println("Content-type:" + localConn.getContentType());
        System.out.println("Content-encoding:" + localConn.getContentEncoding());
        System.out.println("Content-Length:" + localConn.getContentLengthLong());
        
        System.out.println(HttpHeaders.HEADER_REAL_CONTENT_LENGTH + ":" + localConn.getHeaderField(
                HttpHeaders.HEADER_REAL_CONTENT_LENGTH));
        System.out.println("Response-code:" + localConn.getResponseCode());
        System.out.println("Headers:" + localConn.getHeaderFields());

        String movedTo = HttpConnectionUtils.getMovedToLocation(localConn);
        if (movedTo != null) {
            localConn.disconnect();
            return connect(new URL(movedTo));
        }
        
        //got final URL
        contentLength = localConn.getContentLengthLong();
        contentEncoding = localConn.getContentEncoding();
        mimeType = localConn.getContentType();
        
        this.url = url;
        return localConn;
    }
    
}
