package uno.anahata.mapacho.test.app;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import uno.anahata.mapacho.client.LoadingStage.MessageNotification;
import uno.anahata.mapacho.client.LoadingStage.StateNotification;
import uno.anahata.mapacho.client.Mapacho;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;


public class MapachoTestApplication extends Application {

    public void init() throws Exception {
        notifyPreloader(new MessageNotification("Doing some serious init()"));
        Thread.sleep(5000);
        notifyPreloader(new MessageNotification("init() woke up, hiding preloader"));
        hidePreloader();
        Thread.sleep(5000);
        notifyPreloader(new MessageNotification("showing preloader, still in init()"));
        showPreloader();
    }
    
    public void showPreloader() {
        notifyPreloader(StateNotification.SHOW);
    }
    
    public void hidePreloader() {
        notifyPreloader(StateNotification.HIDE);
    }
    
    public void disposePreloader() {
        notifyPreloader(StateNotification.DISPOSE);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        
        notifyPreloader(new MessageNotification("in start(), hiding preloader to show login dialog (fake)"));
        hidePreloader();
        
        Thread.sleep(5000);
        
        notifyPreloader(new MessageNotification("Logged In! (fake)"));
        showPreloader();
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("Mapacho Test Application");
        stage.setScene(scene);
        stage.show();
        Platform.setImplicitExit(true);
        
        Mapacho.log("Notifying preloader to hide");
        
        Platform.runLater(() -> {
            notifyPreloader(new MessageNotification("Sending DISPOSE"));
            super.notifyPreloader(StateNotification.DISPOSE);
        });
        
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
